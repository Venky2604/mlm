import {
    createStore,
    compose,
    applyMiddleware
} from 'redux';
import rootReducer from '../reducers';
import promiseMiddleware from 'redux-promise-middleware';

const middlewares = [];

const addpromiseMiddleware = promiseMiddleware();

middlewares.push(addpromiseMiddleware);

if (process.env.NODE_ENV === `development`) {
    const {
        logger
    } = require(`redux-logger`);

    middlewares.push(logger);
}

const globalStore = createStore(
    rootReducer,
    compose(
      applyMiddleware(...middlewares),
    )
)

export default globalStore