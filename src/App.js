import React, { Component } from 'react';
import './App.css';
import './components/form/forms.css';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from 'react-redux';
import globalStore from './store';
import LoginPage from './components/Home/loginpage';
import Routes from './components/routes';
import ForgotPage from './components/Home/forgotpage';
import ResetPage from './components/Home/resetpage';
import ProtectedRoute from './components/protectedroute';

class App extends Component {
  render() {
    return (
      <Provider store={globalStore}>
        <Router>
          <Switch>
            <Route path="/login" component={LoginPage} />
            <Route path="/forgot-password" component={ForgotPage} />
            <Route path="/reset-password" component={ResetPage} />
            <ProtectedRoute path="/" component={Routes} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
