import axios from "axios";

const domain = "http://mlmlevel.glivade.in/";
// const domain = "http://192.168.1.103:8000/";

const today = new Date();
const currentMonth = today.getMonth() + 1;
const currentYear = today.getFullYear();
const monthQ = `month=${currentMonth}&year=${currentYear}`;

export const urls =  {
    login: domain + "api/auth/login",
    forgotpwd: domain + "api/auth/forgot-password",
    logout: domain + "api/auth/logout",
    resetpwd: domain + "api/auth/reset-password",
    addEpin: domain + "api/epin",
    listUsers: domain + "api/user",
    purchasepin: domain + "api/epin/purchase",
    listAdmins: domain + "api/user/admins",
    listStaffs: domain + "api/user/staffs",
    listCities: domain + "api/state/35",
    allusers: domain + "api/user/users",
    referUser: domain + "api/user/refer",
    exportEpins: domain + "epin/export",
    exportOverallUser: domain + "user/export",
    exportMonthlyUser: domain + `user/export?${monthQ}`,
    levelList: domain + `api/report/level`,
    autoFilling: domain + "api/report/auto-filling",
    dashboard: domain + "api/dashboard/admin",
    autoList: domain + "api/report/auto-filling",
    payoutList: domain + "api/report/auto-filling/payout",
    paymentList: domain + "api/report/auto-filling/payment",
    tours: domain + "api/report/tours",
    exportAuto: domain + "report/auto-filling",
    exportPayout: domain + "report/auto-filling/payout/export",
    exportPayment: domain + "report/auto-filling/payment/export",
    changePwd: domain + "api/auth/change-password",
    authheader: {
        "Content-Type" : "application/json",
        "Accept": "application/json"
    }
};


export const paySelect  = (id) => {
    return { 
        type: "PAY_SELECT",
        id
    }
}

export const openModal = payload => {
    return {
        type : "OPEN_MODAL",
        payload
    }
}

export const closeModal = () => {
    return {
        type : "CLOSE_MODAL"
    }
}

export const showToast = (msg, error = false ) => {
    return {
        type: "SHOW_TOAST",
        msg,
        error
    }
}

export const login = data =>{
    return {
        type: "LOGIN",
        payload: axios({
            url: urls.login,
            method: "POST",
            headers: urls.authheader,
            data: JSON.stringify(data)
        })
    }
}

export const clearUser = () => {
    return { 
        type: "CLEAR_USER"
    }
}

export const resetpwd = (data) => {
    return {
        type: "RESET_PWD",
        payload: axios({
            url: urls.resetpwd,
            method: "POST",
            headers: urls.authheader,
            data: JSON.stringify(data)
        })
    }
}


export const listEpins = (query, token) => {
    return {
        type: "LIST_EPINS",
        payload: axios.get(
            urls.addEpin + (query ? query : "?status=1"), {
            headers: {
                "Accept": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
    }
}

export const listUsers = (params, token, customer) => {
    let key = { 1: "listAdmins", 2: "listStaffs", 3:"listUsers" };
    let { tab, page } = params;
    let usertab = tab ? parseInt(tab, 10) : 1;
    let userq = usertab === 1 ? '' : `&${monthQ}`;
    return {
        type: "LIST_USERS",
        payload: axios.get(urls[key[(customer ? 3 : tab ? tab : 1)]] + (page ? `?page=${page}` : '?page=1') + (customer ? userq : ''), {
            headers: {
                "Accept": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
    }
}

// export const listStaffs = (search, token) => {
//     return {
//         type: "LIST_STAFF",
//         payload: axios.get(urls.listStaffs + search, {
//             headers: {
//                 "Accept": "application/json",
//                 "Authorization": `Bearer ${token}`
//             }
//         })
//     }
// }

// export const listAdmin = (search, token) => {
//     return {
//         type: "LIST_STAFF",
//         payload: axios.get(urls.listStaffs + search, {
//             headers: {
//                 "Accept": "application/json",
//                 "Authorization": `Bearer ${token}`
//             }
//         })
//     }
// }


export const updateUser = payload => {
    return {
        type: "UPDATE_USER",
        payload
    }
}


export const selectEpin = id => {
    return {
        type: "SELECT_EPIN",
        id
    }
}

export const selectListUser = id => {
    return {
        type: "SELECT_LIST_USER",
        id
    }
}

export const clearEpins = () => {
    return {
        type: "CLEAR_EPINS"
    }
}

export const clearListUser = () => {
    return {
        type: "CLEAR_LIST_USER"
    }
}

export const userProfile = (id, token) => {
    return {
        type: "USER_PROFILE",
        payload: axios.get(`${urls.listUsers}/${id}`, {
            headers: {
                "Accept": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
    }
}

export const listLevelsMonth = (tab, token) => {
    let leveltab = tab ? parseInt(tab, 10) : 1;
    return {
        type: "LIST_LEVELS",
        payload: axios.get((leveltab ===  1 ? urls.levelList + `?month=${currentMonth}&year=${currentYear}` : urls.levelList) , {
            headers: {
                "Accept": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
    }
}

export const listTourDetail = (token) => {
    return {
        type: "LIST_TOURS",
        payload: axios.get(urls.tours , {
            headers: {
                "Accept": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
    }
}

export const listSalaryDetail = (token) => {
    return {
        type: "LIST_SALARY",
        payload: axios.get(urls.tours , {
            headers: {
                "Accept": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
    }
}

export const listAuto = (tab, token) => {
    let leveltab = tab ? parseInt(tab, 10) : 1;
    return {
        type: "LIST_AUTO",
        payload: axios.get((leveltab ===  1 ? urls.autoList + `?${monthQ}` : urls.autoList) , {
            headers: {
                "Accept": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
    }
}

export const listLevelsDetail = (id, token) => {
    return {
        type: "LIST_LEVELS_DETAIL",
        payload: axios.get(`${urls.levelList}/${id}`  , {
            headers: {
                "Accept": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
    }
}

export const listAutoDetail = (id, token) => {
    return {
        type: "LIST_AUTO_DETAIL",
        payload: axios.get(`${urls.autoList}/${id}`  , {
            headers: {
                "Accept": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
    }
}

export const clearLevel = () => {
    return {
        type: "LIST_LEVELS_CLEAR"
    }
}

export const levelDetailClear = () => {
    return {
        type: "LIST_LEVELS_DETAIL_CLEAR"
    }
} 

export const autoDetailClear = () => {
    return {
        type: "LIST_AUTO_DETAIL_CLEAR"
    }
} 

export const formOption = (token) => {
    return {
        type: "FORM_OPTIONS",
        payload: Promise.all([
            axios.get(urls.allusers, {
                headers: {
                    "Accept": "application/json",
                    "Authorization": `Bearer ${token}`
                }
            }),
            axios.get(urls.listCities, {
                headers: {
                    "Accept": "application/json",
                    "Authorization": `Bearer ${token}`
                }
            })
        ])
    }
}

export const autoFilling = (token) => {
    return {
        type: "AUTO_FILL_LIST",
        payload: axios.get(`${urls.autoFilling}?${monthQ}`,{
            headers: {
                "Accept": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
    }
}

export const payOutList = (token, payment) => {
    return { 
        type: 'PAYOUT_LIST',
        payload: axios.get(`${!payment ? urls.payoutList : urls.paymentList}`,{
            headers: {
                "Accept": "application/json",
                "Authorization": `Bearer ${token}`
            }
        })
    }
}


/////////////utility functions /////////////////
export const urlParams = url => {
    var keys = {};
    var params = url.replace("?", "").split("&");
    for (var i = 0; i < params.length; i++) {
        var x = params[i].split("=");
        keys[x[0]] = x[1];
    }
    return keys;
}

export const emailvalid = val => {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(val);
}

export const phonevalid = val => {
    var re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    return re.test(val);
}

export const pwdvalid = val => {
    return val.length > 5;
}

export const loadLocalState = () => {
    try {
        const serializedState = sessionStorage.getItem('mlmuser');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
};

export const saveLocalState = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        sessionStorage.setItem('mlmuser', serializedState);
    } catch (err) {
        // Ignore write errors.
    }
};

export const ajaxerrmsg = (res) => {
    var txt = '';
    if (res) {
        let data = res;
        if(data.errors){
            for (var key in data.errors) {
                if (Array.isArray(data.errors[key])) {
                    txt += data.errors[key][0] + " ";
                } else {
                    txt += data.errors[key] + " ";
                }
            }
        } else {
            txt += data.message;
        }
    } else {
        txt = "Something went wrong";
    }
    return txt;
}

export const dateTimeFormat = (x) => {
    return new Date(x).toLocaleString('en-IN', { year: 'numeric', day: '2-digit', month: 'short', hour: 'numeric', minute: 'numeric', hour12: true });
};

export const formstate = (inputs) => {
    let state = {};
    for(let i = 0; i < inputs.length; i++){
        state[inputs[i].name] = "";
        state[inputs[i].name + "_err"] = "";
    }
    return state
}