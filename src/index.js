import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import "./assets/vendors/base/vendors.bundle.css";
import "./assets/demo/default/base/style.bundle.css";
import "./assets/css/custom.css";
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
