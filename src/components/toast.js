import React, { Component } from 'react'
import Portal from './portal';
import { connect } from "react-redux";
import {hideToast} from "../actions/common"

class Toast extends Component {

    componentWillReceiveProps(nextProps){
        if(nextProps.show){
            setTimeout(function(){
                nextProps.dispatch(hideToast());
            }, 5000)
        }
    }

    render() {
        let { show,  msg } = this.props;
        return (
            show ?
            <Portal>
                <div className="toast flex-center">
                    <div className="toast-msg">
                        {msg}
                    </div>
                </div>
            </Portal>
            : null
        )
    }
}

export default connect((state)=> state.toast)(Toast)