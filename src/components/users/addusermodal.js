import React, { Component } from 'react'
import MInput from '../form/minput';
import { connect } from 'react-redux';
import ModalClose from "../closemodal";
import { phonevalid,  formstate, urls, ajaxerrmsg, userProfile, openModal } from '../../actions/common';
import axios from "axios";

const inputs = [
    {name: "name", type: "text", label: "Name", required: true},
    {name: "email", type: "email", label: "Email", required: false},
    {name: "phone", type: "tel", label: "Mobile Number", maxLength: 10, required: true},
    {name: "address", type: "text", label: "Address",  required: true},
    {name: "city_id", type: "select", label: "Select City", optionName: "city", optionValue: "id", options: "cities",  required: true},
    {name: "postal_code", type: "tel", label: "Pin code", maxLength: 6,  required: true},
    {name: "epin", type: "text", label: "Enter E-Pin",  required: true}
];


class AddUserModal extends Component {

    constructor(props){
        super(props);
        this.state = {
            ...formstate(inputs),
            "error": false,
            "fetching": false
        };
        this.handleChange = this.handleChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this);  
        this.handleOption = this.handleOption.bind(this);  
        this.submitForm = this.submitForm.bind(this);  
    }


    handleChange(e){
        let {name, value} = e.target;
        this.setState({[name]: value, [name + "_err"]: ''})
    }

    handleOption(value, name){
        this.setState({[name]: value, [name + "_err"]: ''})
    }

    handleSubmit(e){
        e.preventDefault();
        let { epin, address, postal_code, city_id ,email, name, phone } = this.state;
        let { user_id } = this.props;
        if(!phonevalid(phone)){
            this.setState({ "phone_err": "Mobile number is invalid" });
            return;
        }
        this.submitForm({
            epin, address, postal_code, city_id ,email, name, phone, user_id
        })
    }

    async submitForm(data){
        this.setState({fetching: true});
        try {
            let {user, dispatch, user_id} = this.props;
            let res = await axios({
                url: urls.referUser,
                method: "post",
                headers: {
                    "Content-Type" : "application/json",
                    "Accept": "application/json",
                    "Authorization": `Bearer ${user.bearer_token}`
                },
                data: JSON.stringify(data)
            });
            if(res){
                console.log(res);
                this.setState({fetching: false});   
                dispatch( userProfile(user_id, user.bearer_token) );
                dispatch(openModal({ center: true, modalName: "USER_SUCC", username: res.data.username, pwd: res.data.raw_password , head: "ID and Password"}));            
            }
        } catch(error){
            this.setState({fetching: false});   
            this.setState({error: ajaxerrmsg(error.response.data)});            
        }
    }

    render() {
        let state = this.state;
        let { FormOptions, spName, spId } = this.props;
        let { epin, address, postal_code, city_id , name, phone, error, fetching } = state;
        return (
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLongTitle">{this.props.head}</h5>
                    <ModalClose className="close">
                        <span aria-hidden="true">×</span>
                    </ModalClose>
                </div>
                <div className="modal-body">
                    <div className="m-login__form m-form">
                        <div className="row">
                            {
                                inputs.map((ip, index) => 
                                    <MInput 
                                        label={ip.label}
                                        inputprops={{type: ip.type, placeholder: `Enter ${ip.label}`, name: ip.name, maxLength: ip.maxLength, value: state[ip.name], optionname: ip.optionName, optionvalue: ip.optionValue, options: FormOptions[ip.options] ? FormOptions[ip.options] : [] }}
                                        handleChange={ip.type === "select" ? this.handleOption : this.handleChange}
                                        error={state[ip.name + "_err"]}
                                        key={`input-${index}-${ip.name}`}
                                        half={ip.full ? false :true}
                                        required={ip.required}
                                    />
                                )
                            }
                            <div className={"form-group m-form__group mb0 pb0 col-lg-6"}>
                                <p className="geneologytext mt-3">Sponsor Name: <span>{spName}</span></p>
                                <p className="geneologytext">Sponsor ID: <span>{spId}</span></p>
                            </div>
                            {error &&<div className={"col-md-12"}><p className="errtxt">{error}</p></div>}
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <ModalClose className="btn btn-secondary">Close</ModalClose>
                    <button disabled={ (epin  && address  && postal_code  && city_id   && name  && phone  && !fetching ) ? false : true} onClick={this.handleSubmit} type="submit" className="btn btn-primary bgbtn">Submit  {fetching && <div className="sp-circle"></div>}</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    let { user, FormOptions } = state;
    return {
        user: user.data,
        FormOptions: FormOptions
    }
}

export default connect(mapStateToProps)(AddUserModal)