import React, { Component } from 'react'
import ListingHOC from '../listinghoc';
import { tableHOC } from '../tableHoc';
import { openModal, dateTimeFormat, selectListUser } from '../../actions/common';
import { Link } from "react-router-dom";

class UserItem extends Component {

    selectUser(modalName , update){
        let { user_profile, id, dispatch} = this.props;
        dispatch(selectListUser(id));
        dispatch(openModal({center: true, modalName, head: `${update ? 'Update ' : ''}${user_profile.name} Details`}))
    }

    render() {
        let {slno, phone, user_profile, dispatch, id, created_at, referrer, username} = this.props;
        return (
            <tr>
                <td>{slno }</td>
                <td><Link to={`/tree?user=${id}`}>{username}</Link></td>
                <td><Link to={`/tree?user=${id}`}>{user_profile.name}</Link></td>
                <td>{phone}</td>
                <td>{referrer ? <Link to={`/tree?user=${referrer.referrer.user_profile.user_id}`}>{referrer.referrer.user_profile.name}</Link> : 'Company'}</td>
                <td>{referrer ? <Link to={`/tree?user=${referrer.referrer.user_profile.user_id}`}>{referrer.referrer.username}</Link> : 'kps10000'}</td>
                <td>{dateTimeFormat(created_at)}</td>
                <td><a className="tablesmallbtn" onClick={() => dispatch(openModal({center: true, modalName: "RESET_MODAL", head: "Reset Passsword", user_id: id}))}>Click Here</a></td>
                <td>
                    <a className="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill infobtn mr-2" onClick={() => this.selectUser("USER_DETAILS_MODAL", false)}><i className="la la-info"></i></a>
                    <a className="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill infobtn mr-2" onClick={() => this.selectUser("UPDATE_USER_MODAL", true)}><i className="la la-edit"></i></a>
                </td>
            </tr>
        )
    }
}

const heading = ["S.No.", "Customer ID", "Name", "Mobile No.", "Sponsor", "Sponsor ID", "Joined on", "Reset Password", "Actions"];

export default tableHOC(ListingHOC(UserItem, "listUsers"), "listUsers", heading)
