import React, { Component } from 'react'
import {connect} from "react-redux"
import { listUsers, formOption, urlParams, urls, clearListUser } from '../../actions/common';
import Pagination from '../pagination';
import UserItem from './useritem';

class CustomersPage extends Component {

    componentDidMount() {
        let { user, dispatch, location } = this.props;
        dispatch(formOption(user.bearer_token));
        dispatch(listUsers(urlParams(location.search), user.bearer_token, true));
    }

    changeTab(tab) {
        let { history, listUsers } = this.props;
        if(listUsers.fetching){
            return;
        }
        history.replace(`/customer-management?tab=${tab}`);
    }

    componentWillReceiveProps(nextProps){
        let { location, user, dispatch } = nextProps
        if(nextProps.location.search !== this.props.location.search ){
            dispatch(listUsers(urlParams(location.search), user.bearer_token, true));
        }
    }

    render() {
        let { current_page, last_page, data, fetching} = this.props.listUsers;
        let params = urlParams(this.props.location.search);
        let tab = params.tab ? parseInt(params.tab, 10) : 1; 
        // let history = this.props.history;
        return (
            <div className="m-grid__item m-grid__item--fluid m-wrapper">
                <div className="m-content">
                    <div className="m-portlet m-portlet--mobile">
                        <div className="row tableheaderrow">
                            <div className="col-md-4 col-sm-3">
                                <h3 className="m-portlet__head-text tableheading">
                                    Customer
                                </h3>
                            </div>
                            <div className="col-md-8 col-sm-9">
                                <div className="input-group">
                                    <input type="text" className="form-control form-control-warning searchinput" placeholder="Search by Name, Mobile Number..." />
                                    <div className="input-group-append">
                                        <button className="btn btn-brand searchbtn" type="button">Go!</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="m-portlet__body pb60">
                            <div className="row">
                                <div className="col-lg-8">
                                    <ul className="nav nav-tabs  m-tabs-line" role="tablist">
                                        <li className="nav-item m-tabs__item">
                                            <a onClick={() => this.changeTab(1)} className={`nav-link m-tabs__link ${tab === 1 ? "active" : ""}  ${fetching ? "disabledpointer" : ''}`} >Overall</a>
                                        </li>
                                        <li className="nav-item m-tabs__item">
                                            <a onClick={() => this.changeTab(2)} className={`nav-link m-tabs__link ${tab === 2 ? "active" : ""} ${fetching ? "disabledpointer" : ''}`}>Current Month</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-lg-2 pr5 floatright">
                                    <a href={urls.exportOverallUser} className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright ml5 width100">
                                    <span>Export Overall</span>
                                    </a>
                                </div>
                                <div className="col-lg-2 plr5 floatright">
                                    <a href={urls.exportMonthlyUser} className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright ml5 width100">
                                    <span>Export Monthly</span>
                                    </a>
                                </div>
                            </div>
                            <div className="tab-content">
                                <div className="tab-pane active">
                                    {
                                        <UserItem />
                                    } 
                                </div>
                                {data.length !== 0 ? 
                                <Pagination current={current_page} last={last_page} pagesCount={5} 
                                location={`/customer-management?tab=${tab}`} />: null}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentWillUnmount(){
        this.props.dispatch(clearListUser());
    }
}

const mapStateToProps = (state) => {
    return { 
        user: state.user.data,
        listUsers: state.listUsers
    }
}

export default connect(mapStateToProps)(CustomersPage)