import React, { Component } from 'react'
import MInput from '../form/minput';
import { connect } from 'react-redux';
import ModalClose from "../closemodal";
import { pwdvalid, formstate, urls, ajaxerrmsg, closeModal } from '../../actions/common';
import axios from "axios";

const inputs = [
    {name: "my_password", type: "password", label: "User Password"},
    {name: "user_password", type: "password", label: "New Password"},    
    {name: "confirm_password", type: "password", label: "Retype New Password"},    
];


class ResetUserPwd extends Component {

    constructor(props){
        super(props);
        this.state = {
            ...formstate(inputs),
            "error": false,
            "fetching": false
        };
        this.handleChange = this.handleChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this);  
        this.submitForm = this.submitForm.bind(this);  
    }


    handleChange(e){
        let {name, value} = e.target;
        this.setState({[name]: value, [name + "_err"]: ''})
    }

    handleOption(value, name){
        this.setState({[name]: value, [name + "_err"]: ''})
    }

    handleSubmit(e){
        e.preventDefault();
        console.log("not validated");
        let { user_password, my_password, confirm_password } = this.state;
        if(!pwdvalid(my_password)){
            this.setState({ "my_password_err": "Password must be atleast 6 characters" });
            return;
        }
        if(!pwdvalid(user_password)){
            this.setState({ "user_password_err": "Password must be atleast 6 characters" });
            return;
        }
        if(!pwdvalid(confirm_password)){
            this.setState({ "confirm_password_err": "Password must be atleast 6 characters" });
            return;
        }
        if(confirm_password !== user_password){
            this.setState({ "confirm_password_err": "Passwords do not match" });
            return;
        }
        this.submitForm({
            user_password, my_password
        })
    }

    async submitForm(data){
        this.setState({fetching: true});
        try {
            let {user, dispatch} = this.props;
            let res = await axios({
                url: urls.listUsers + "/" + this.props.user_id + "/reset-password",
                method: "post",
                headers: {
                    "Content-Type" : "application/json",
                    "Accept": "application/json",
                    "Authorization": `Bearer ${user.bearer_token}`
                },
                data: JSON.stringify(data)
            });
            if(res){
                console.log(res);
                this.setState({fetching: false});   
                dispatch(closeModal());            
            }
        } catch(error){
            this.setState({fetching: false});   
            this.setState({error: ajaxerrmsg(error.response.data)});            
        }
    }

    render() {
        let state = this.state;
        let { user_password, my_password, confirm_password, error, fetching } = state;
        return (
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLongTitle">{this.props.head}</h5>
                    <ModalClose className="close">
                        <span aria-hidden="true">×</span>
                    </ModalClose>
                </div>
                <div className="modal-body">
                    <div className="m-login__form m-form">
                        <div className="row">
                            {
                                inputs.map((ip, index) => 
                                    <MInput 
                                        label={ip.label}
                                        inputprops={{type: ip.type, placeholder: `Enter ${ip.label}`, name: ip.name, value: state[ip.name] }}
                                        handleChange={this.handleChange}
                                        error={state[ip.name + "_err"]}
                                        key={`input-${index}-${ip.name}`}
                                        half={false}
                                    />
                                )
                            }
                            {error &&<div className={"col-md-12"}><p className="errtxt">{error}</p></div>}
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <ModalClose className="btn btn-secondary">Close</ModalClose>
                    {console.log()}
                    <button disabled={ ( user_password && my_password && confirm_password && !fetching) ? false : true} onClick={this.handleSubmit} type="submit" className="btn btn-primary bgbtn">Submit  {fetching && <div className="sp-circle"></div>}</button>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    let { user } = state;
    return {
        user: user.data
    }
}

export default connect(mapStateToProps)(ResetUserPwd)