import React, { Component } from 'react'
import MInput from '../form/minput';
import { connect } from 'react-redux';
import ModalClose from "../closemodal";
import { emailvalid, phonevalid, formstate, urls, ajaxerrmsg, closeModal } from '../../actions/common';
import axios from "axios";

const inputs = [
    {name: "first_name", type: "text", label: "First name"},
    {name: "last_name", type: "text", label: "Last name"},
    {name: "email", type: "email", label: "Email"},
    {name: "phone", type: "tel", label: "Mobile Number"}
];


class AddStaff extends Component {

    constructor(props){
        super(props);
        this.state = {
            ...formstate(inputs),
            "error": false,
            "fetching": false
        };
        this.handleChange = this.handleChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this);  
        this.submitForm = this.submitForm.bind(this);  
    }

    handleChange(e){
        let {name, value} = e.target;
        this.setState({[name]: value, [name + "_err"]: ''})
    }

    handleSubmit(e){
        e.preventDefault();
        console.log("not validated");
        let { email, first_name, last_name, phone } = this.state;
        if(!emailvalid(email)){
            this.setState({ "email_err": "Email is invalid" });
            return;
        }
        if(!phonevalid(phone)){
            this.setState({ "phone_err": "Mobile number is invalid" });
            return;
        }
        console.log("validated");
        this.submitForm({
            email, first_name, last_name, phone, "role_id": this.props.role_id
        })
    }

    async submitForm(data){
        console.log("validated");
        this.setState({fetching: true});
        try {
            let {user, dispatch} = this.props;
            let res = await axios({
                url: urls.listUsers,
                method: "post",
                headers: {
                    "Content-Type" : "application/json",
                    "Accept": "application/json",
                    "Authorization": `Bearer ${user.bearer_token}`
                },
                data: JSON.stringify(data)
            });
            if(res){
                console.log(res);
                this.setState({fetching: false});   
                dispatch(closeModal());            
            }
        } catch(error){
            this.setState({fetching: false});   
            this.setState({error: ajaxerrmsg(error.response.data.errors)});            
        }
    }

    render() {
        let state = this.state;
        let { email, first_name, last_name, phone, error, fetching } = state;
        return (
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLongTitle">{this.props.head}</h5>
                    <ModalClose className="close">
                        <span aria-hidden="true">×</span>
                    </ModalClose>
                </div>
                <div className="modal-body">
                    <div className="m-login__form m-form">
                        <div className="row">
                            {
                                inputs.map((ip, index) => 
                                    <MInput 
                                        label={ip.label}
                                        inputprops={{type: ip.type, placeholder: `Enter ${ip.label}`, name: ip.name, value: state[ip.name] }}
                                        handleChange={this.handleChange}
                                        error={state[ip.name + "_err"]}
                                        key={`input-${index}-${ip.name}`}
                                    />
                                )
                            }
                            {error &&<div className={"col-md-12"}><p className="errtxt">{error}</p></div>}
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <ModalClose className="btn btn-secondary">Close</ModalClose>
                    {console.log()}
                    <button disabled={ (email && first_name && last_name &&  phone && !fetching) ? false : true} onClick={this.handleSubmit} type="submit" className="btn btn-primary bgbtn">Submit  {fetching && <div className="sp-circle"></div>}</button>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        user: state.user.data
    }
}

export default connect(mapStateToProps)(AddStaff)