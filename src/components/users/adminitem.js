import React, { Component } from 'react'
import ListingHOC from '../listinghoc';
import { tableHOC } from '../tableHoc';
import { openModal } from '../../actions/common';

class AdminItem extends Component {
    render() {
        let {slno, page, phone, user_profile, dispatch, id} = this.props;
        console.log(page);
        return (
            <tr>
                <td>{slno}</td>
                <td>{user_profile.name}</td>
                <td>{phone}</td>
                <td><a className="tablesmallbtn"  onClick={() => dispatch(openModal({center: true, modalName: "RESET_MODAL", head: "Reset Passsword", user_id: id}))}>Click Here</a></td>
            </tr>
        )
    }
}

const heading = ["S.No.", "Username", "Mobile No.", "Reset Password"];

export default tableHOC(ListingHOC(AdminItem, "listUsers"), "listUsers", heading)