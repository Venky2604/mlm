import React, { Component } from 'react'
import {connect} from "react-redux"
import { listUsers, openModal, formOption, urlParams, clearListUser } from '../../actions/common';
import AdminItem from './adminitem';
import Pagination from '../pagination';

class UsersPage extends Component {

    componentDidMount() {
        let { user, dispatch, location } = this.props;
        dispatch(formOption(user.bearer_token));
        dispatch(listUsers(urlParams(location.search), user.bearer_token));
    }

    changeTab(tab) {
        let { history, listUsers } = this.props;
        if(listUsers.fetching){
            return;
        }
        history.replace(`/admin-management?tab=${tab}`);
    }

    componentWillReceiveProps(nextProps){
        let { location, user, dispatch } = nextProps
        if(nextProps.location.search !== this.props.location.search ){
            dispatch(listUsers(urlParams(location.search), user.bearer_token));
        }
    }

    render() {
        let { current_page, last_page, data, fetching} = this.props.listUsers;
        let params = urlParams(this.props.location.search);
        let tab = params.tab ? parseInt(params.tab, 10) : 1; 
        let history = this.props.history;
        return (
            <div className="m-grid__item m-grid__item--fluid m-wrapper">
                <div className="m-content">
                    <div className="m-portlet m-portlet--mobile">
                        <div className="row tableheaderrow">
                            <div className="col-md-4 col-sm-3">
                                <h3 className="m-portlet__head-text tableheading">
                                    Admin/Staff Management
                                </h3>
                            </div>
                            <div className="col-md-8 col-sm-9">
                                <div className="input-group">
                                    <input type="text" className="form-control form-control-warning searchinput" placeholder="Search by Name, Mobile Number..." />
                                    <div className="input-group-append">
                                        <button className="btn btn-brand searchbtn" type="button">Go!</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="m-portlet__body pb60">
                            <div className="row">
                                <div className="col-lg-6">
                                    <ul className="nav nav-tabs  m-tabs-line" role="tablist">
                                        <li className="nav-item m-tabs__item">
                                            <a onClick={() => this.changeTab(1)} className={`nav-link m-tabs__link ${tab === 1 ? "active" : ""}  ${fetching ? "disabledpointer" : ''}`} >Admins</a>
                                        </li>
                                        <li className="nav-item m-tabs__item">
                                            <a onClick={() => this.changeTab(2)} className={`nav-link m-tabs__link ${tab === 2 ? "active" : ""} ${fetching ? "disabledpointer" : ''}`}>Staffs</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-lg-2 pr5 floatright"></div>
                                <div className="col-lg-2 pr5 floatright">
                                    <a className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright mr5 width100" onClick={()=> this.props.dispatch(openModal({history, center: true, modalName: "ADD_ADMIN_MODAL", head: "Add Admin", role_id: 2}))}>
                                    <span><i className="la la-plus"></i><span>Add Admin</span></span>
                                    </a>
                                    
                                </div>
                                <div className="col-lg-2 plr5 floatright">
                                    <a data-toggle="modal" data-target="#addstaffmodal" className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright ml5 width100"  onClick={()=> this.props.dispatch(openModal({history, center: true, modalName: "ADD_ADMIN_MODAL", head: "Add Staff", role_id: 3}))}>
                                    <span><i className="la la-plus"></i><span>Add Staff</span></span>
                                    </a>
                                </div>
                            </div>
                            <div className="tab-content">
                                <div className="tab-pane active">
                                    {
                                        <AdminItem />
                                    } 
                                </div>
                                {data.length !== 0 ? 
                                <Pagination current={current_page} last={last_page} pagesCount={5} 
                                location={`/admin-management?tab=${tab}`} />: null}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentWillUnMount(){
        this.props.dispatch(clearListUser());
    }
}

const mapStateToProps = (state) => {
    return { 
        user: state.user.data,
        listUsers: state.listUsers
    }
}

export default connect(mapStateToProps)(UsersPage)