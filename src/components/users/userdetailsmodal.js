import React, { Component } from 'react'
import ModalClose from "../closemodal";
import { connect } from "react-redux"

class UserDetailsModal extends Component {
    render() {
        let {phone, email, user_profile, referrer, username, user_address, direct_referral_count, downline_count} = this.props.userselected;
        return (
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLongTitle">{this.props.head}</h5>
                    <ModalClose className="close">
                        <span aria-hidden="true">×</span>
                    </ModalClose>
                </div>
                <div className="modal-body">
                    <div className="m-login__form m-form">
                        <div className="row">
                            <div className="col-md-6">
                                <p className="geneologytext">Name : <span>{user_profile.name}</span></p>
                            </div>
                            <div className="col-md-6">
                                <p className="geneologytext">Customer ID : <span>{username}</span></p>
                            </div>
                            <div className="col-md-6">
                                <p className="geneologytext">Sponsor Name : <span>{referrer ? referrer.referrer.user_profile.name : "Company"}</span></p>
                            </div>
                            <div className="col-md-6">
                                <p className="geneologytext">Sponsor ID : <span>{referrer ? referrer.referrer.username : "KPS0000"}</span></p>
                            </div>
                            <div className="col-md-6">
                                <p className="geneologytext">Direct Line : <span>{direct_referral_count}</span></p>
                            </div>
                            <div className="col-md-6">
                                <p className="geneologytext">Total Downline : <span>{downline_count}</span></p>
                            </div>
                            <div className="col-md-6">
                                <p className="geneologytext">Email : <span>{email}</span></p>
                            </div>
                            <div className="col-md-6">
                                <p className="geneologytext">Phone number : <span>{phone}</span></p>
                            </div>
                            <div className="col-md-12">
                                <p className="geneologytext">Address : <span>{`${user_address.address}, ${user_address.postal_code}`}</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

const mapStateToProps = state => {
    return {
        userselected: state.listUsers.userselected
    }
}

export default connect(mapStateToProps)(UserDetailsModal)