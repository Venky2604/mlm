import React, { Component } from 'react'
import MInput from '../form/minput';
import { connect } from 'react-redux';
import ModalClose from "../closemodal";
import { emailvalid, phonevalid,  formstate, urls, ajaxerrmsg, closeModal, updateUser } from '../../actions/common';
import axios from "axios";

const inputs = [
    {name: "name", type: "text", label: "Name"},
    {name: "email", type: "email", label: "Email"},
    {name: "phone", type: "tel", label: "Mobile Number", maxLength: 10},
    {name: "address", type: "text", label: "Address"},
    {name: "city_id", type: "select", label: "Select City", optionName: "city", optionValue: "id", options: "cities", city: true},
    {name: "postal_code", type: "tel", label: "Pin code", maxLength: 6},
];


class UpdateUserModal extends Component {

    constructor(props){
        super(props);
        this.state = {
            ...formstate(inputs),
            address: props.userselected.user_address.address, 
            postal_code: props.userselected.user_address.postal_code, 
            city_id: props.userselected.user_address.city_id,
            email: props.userselected.email, 
            name: props.userselected.user_profile.name, 
            phone: props.userselected.phone,
            "error": false,
            "fetching": false
        };
        this.handleChange = this.handleChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this);  
        this.handleOption = this.handleOption.bind(this);  
        this.submitForm = this.submitForm.bind(this);  
    }

    handleChange(e){
        let {name, value} = e.target;
        this.setState({[name]: value, [name + "_err"]: ''})
    }

    handleOption(value, name){
        this.setState({[name]: value, [name + "_err"]: ''})
    }

    handleSubmit(e){
        e.preventDefault();
        let { address, postal_code, city_id ,email, name, phone } = this.state;
        console.log(this.props);
        if(!emailvalid(email)){
            this.setState({ "email_err": "Email is invalid" });
            return;
        }
        if(!phonevalid(phone)){
            this.setState({ "phone_err": "Mobile number is invalid" });
            return;
        }
        console.log("validated");
        this.submitForm({
            address, postal_code, city_id ,email, name, phone
        })
    }

    async submitForm(data){
        console.log("validated");
        this.setState({fetching: true});
        try {
            let {user, dispatch, userselected} = this.props;
            let res = await axios({
                url: `${urls.listUsers}/${userselected.id}`,
                method: "put",
                headers: {
                    "Content-Type" : "application/json",
                    "Accept": "application/json",
                    "Authorization": `Bearer ${user.bearer_token}`
                },
                data: JSON.stringify(data)
            });
            if(res){
                console.log(res);
                this.setState({fetching: false});   
                //dispatch list users 
                dispatch(updateUser(res.data));
                dispatch(closeModal());            
            }
        } catch(error){
            this.setState({fetching: false});   
            console.log(error.response);
            this.setState({error: ajaxerrmsg(error.response.data)});            
        }
    }

    render() {
        let state = this.state;
        let { FormOptions } = this.props;
        let { address, postal_code, city_id ,email, name, phone, error, fetching } = state;
        return (
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLongTitle">{this.props.head}</h5>
                    <ModalClose className="close">
                        <span aria-hidden="true">×</span>
                    </ModalClose>
                </div>
                <div className="modal-body">
                    <div className="m-login__form m-form">
                        <div className="row">
                            {
                                !FormOptions.fetching &&
                                inputs.map((ip, index) => 
                                    <MInput 
                                        label={ip.label}
                                        inputprops = {{
                                            type: ip.type,
                                            placeholder: `Enter ${ip.label}`,
                                            name: ip.name,
                                            maxLength: ip.maxLength,
                                            value: state[ip.name],
                                            optionname: ip.optionName,
                                            optionvalue: ip.optionValue,
                                            options: FormOptions[ip.options] ? FormOptions[ip.options] : [],
                                            selectedvalue: ip.city ? FormOptions.cities.filter(x => x.id === city_id)[0] : null
                                        }}
                                        handleChange={ip.type === "select" ? this.handleOption : this.handleChange}
                                        error={state[ip.name + "_err"]}
                                        key={`input-${index}-${ip.name}`}
                                        half={ip.full ? false :true}
                                    />
                                )
                            }
                            {error &&<div className={"col-md-12"}><p className="errtxt">{error}</p></div>}
                        </div>
                    </div>
                </div>
                <div className="modal-footer">
                    <ModalClose className="btn btn-secondary">Close</ModalClose>
                    <button disabled={ (address  && postal_code  && city_id   &&email  && name  && phone  && !fetching ) ? false : true} onClick={this.handleSubmit} type="submit" className="btn btn-primary bgbtn">Submit  {fetching && <div className="sp-circle"></div>}</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    let { user, FormOptions, listUsers } = state;
    return {
        user: user.data,
        FormOptions: FormOptions,
        userselected: listUsers.userselected
    }
}

export default connect(mapStateToProps)(UpdateUserModal)