import React, {Component} from 'react'
import PinItem from './pinitem';
import Pagination from '../pagination';
import {connect} from "react-redux"
import { openModal, listEpins, urlParams, clearEpins, formOption, urls } from '../../actions/common';

class EpinsPage extends Component{

    componentDidMount(){
        let { user, dispatch, location } = this.props;
        dispatch(listEpins(location.search, user.bearer_token));
        dispatch(formOption(user.bearer_token));
    }

    componentWillReceiveProps(nextProps){
        let { dispatch, location, user } = nextProps;
        if(this.props.location.search !== location.search){
            dispatch(listEpins(location.search, user.bearer_token));
        }
    }

    changeTab(tab){
        if(this.props.epins.fetching){
            return;
        }
        this.props.history.replace(`/epins?status=${tab}`);
    }

    render(){
        let search = urlParams(this.props.location.search);
        let status = parseInt(search.status ? search.status : 1 , 10);
        let epins = this.props.epins;
        return (
            <div className="m-grid__item m-grid__item--fluid m-wrapper">
                <div className="m-content">
                    <div className="m-portlet m-portlet--mobile">
                        <div className="row tableheaderrow">
                            <div className="col-md-4 col-sm-3">
                                <h3 className="m-portlet__head-text tableheading">
                                    E-Pin
                                </h3>
                            </div>
                            <div className="col-md-6 col-sm-6 pr0">
                                <div className="input-group">
                                    <input type="text" className="form-control form-control-warning searchinput" placeholder="Search by E-Pin..." />
                                    <div className="input-group-append">
                                        <button className="btn btn-brand searchbtn" type="button">Go!</button>
                                    </div>
                                </div>
                            </div>
                            {this.props.user.user.role.id === 1 ? <div className="col-md-2 col-sm-3 pr0">
                                <a id="m_quick_sidebar_toggle" className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright width100" onClick={()=> this.props.dispatch(openModal({center: false, modalName: "GENERATE_PIN"}))}>
                                    <span>
                                    <i className="la la-plus"></i>
                                    <span>Generate E-Pin</span>
                                    </span>
                                </a>
                            </div>: null}
                        </div>
                        <div className="m-portlet__body pb60">
                            <div className="row">
                                <div className="col-md-10">
                                    <ul className="nav nav-tabs  m-tabs-line" role="tablist">
                                        <li className="nav-item m-tabs__item">
                                            <a onClick={() => this.changeTab(1)} className={`nav-link m-tabs__link ${status === 1 ? "active" : ""} ${this.props.epins.fetching ? "disabledpointer" : ''}`} data-toggle="tab" role="tab">Available Pins</a>
                                        </li>
                                        <li className="nav-item m-tabs__item">
                                            <a onClick={() => this.changeTab(2)} className={`nav-link m-tabs__link ${status === 2 ? "active" : ""}  ${this.props.epins.fetching ? "disabledpointer" : ''}`} data-toggle="tab" role="tab">Assigned Pins</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-lg-2 plr5 floatright">
                                    <a href={urls.exportEpins} className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright ml5 width100">
                                    <span>Export E-pins</span>
                                    </a>
                                </div>
                            </div>
                            <div className="tab-content">
                                <div className="tab-pane active" id="m_tabs_1_1" role="tabpanel">
                                    <div className="row">
                                        <PinItem />
                                    </div>
                                </div>
                            </div>
                            {epins.data.length !== 0 && !epins.fetching ? 
                            <Pagination current={epins.current_page} last={epins.last_page} pagesCount={5} 
                            location={`/epins?status=${status}`} />: null}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentWillUnmount(){
        this.props.dispatch(clearEpins());
    }
}

const mapStateToProps = (state) => {
    return { 
        user: state.user.data,
        epins: state.epins
    }
}

export default connect(mapStateToProps)(EpinsPage)
