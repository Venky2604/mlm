import React, { Component } from 'react';
import { openModal, selectEpin } from '../../actions/common';
import ListingHOC from '../listinghoc';

class PinItem extends Component {

    selectPin(id){
        let { dispatch } = this.props; 
        dispatch(selectEpin(id));
        dispatch(openModal({center: true, modalName: "ASSIGN_PIN"}))
    }

    render() {
        let { epin,  id, epin_status_id, epin_purchase, user_referral} = this.props;
        return (
            <div className="col-lg-3">
                <div className="epinsection">
                    <p align="center" className="btn-code mt0" onClick={() => this.selectPin(id)}>
                        <span className="partial-code">{epin}</span> 
                        <span className="btn-hover">View Pin</span>
                    </p>
                    {epin_status_id === 2 ? 
                        <p className="createddate theme-color-text">{epin}</p> : null
                    }
                    {
                        epin_purchase && <p className="createddate">Purchased By {epin_purchase.user.user_profile.name}</p>
                    }
                    {
                        user_referral && <p className="createddate">Refferal: {user_referral.user.user_profile.name}</p>
                    }
                </div>
            </div>
        )
    }
}

export default ListingHOC(PinItem, "epins")