import React, { Component } from 'react'
import NumberOnlyInput from '../form/numberonlyinput';
import { urls, ajaxerrmsg, closeModal, listEpins } from '../../actions/common';
import { connect } from 'react-redux';
import axios from "axios";

class AddPinForm extends Component {

    constructor(props){
        super(props);
        this.state = {
            count: '',
            error: false,
            fetching: false
        };
        this.handleChange = this.handleChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this); 
        this.addPin = this.addPin.bind(this); 
    }

    handleChange(e){
        let {name, value} = e.target;
        this.setState({[name]: value});
    }

    handleSubmit(e){
        e.preventDefault();
        this.addPin({
            "count": this.state.count,
	        "amount": 500
        })
    }

    async addPin(data){
        this.setState({fetching: true});
        try {
            let {user, dispatch} = this.props;
            let res = await axios({
                url: urls.addEpin,
                method: "post",
                headers: {
                    "Content-Type" : "application/json",
                    "Accept": "application/json",
                    "Authorization": `Bearer ${user.bearer_token}`
                },
                data: JSON.stringify(data)
            });
            if(res){
                console.log(res);
                this.setState({fetching: false});            
                dispatch(listEpins(0, user.bearer_token));
                dispatch(closeModal());            
            }
        } catch(error){
            this.setState({fetching: false});            
            this.setState({error: ajaxerrmsg(error.response.data)});            
        }
    }

    render() {
        let { count, fetching , error} = this.state;
        return (
            <form className="m-login__form m-form" onSubmit={this.handleSubmit}>
                <div className="row">
                    <div className="form-group m-form__group mb0 pb0 mt15 col-lg-12">
                        <label className="labelheading">E-Pin Amount&nbsp;<span className="required">*</span></label>
                        <br />
                        <input className="form-control m-input" type="text" placeholder="E-Pin Amount" value={500} readOnly/>
                    </div>
                    <div className="form-group m-form__group mb0 pb0 col-lg-12">
                        <label className="labelheading">No. of E-Pins&nbsp;<span className="required">*</span></label>
                        <NumberOnlyInput value={count} name="count" onChange={this.handleChange} className="form-control m-input" type="text" placeholder="No. of E-Pins" maxLength="3" />
                    </div>
                    {error &&<div className="form-group m-form__group mb0 pb0 col-lg-12"><p className="errtxt">{error}</p></div>}
                    <div className="m-login__form-action mt15 col-lg-12">
                        <center><button disabled={count ? false : true}  onSubmit={this.handleSubmit} type="submit" className="btn btn-focus m-btn width100">Generate Pin  {fetching && <div className="sp-circle"></div>}</button></center>
                    </div>
                </div>
            </form>
        )
    }
}

const mapStateToProps = (state) => {
    return { 
        user: state.user.data
    }
}

export default connect(mapStateToProps)(AddPinForm)
