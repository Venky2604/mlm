import React, { Component } from 'react'
import ModalClose from "../closemodal"
import AddPinForm from "./addpinform"

class GeneratePinModal extends Component {
    render() {
        return (
            <div className="m-quick-sidebar__content">
                <ModalClose className="close m-quick-sidebar__close"><i className="la la-close"></i></ModalClose>
                <ul id="m_quick_sidebar_tabs" className="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand mb5" role="tablist">
                    <li className="nav-item m-tabs__item">
                        <a className="nav-link m-tabs__link active tabanchor" data-toggle="tab">Generate E-Pin</a>
                    </li>
                </ul>
                <div className="tab-content">
                    <div className="tab-pane active" id="m_quick_sidebar_tabs_messenger" role="tabpanel">
                        <AddPinForm />
                    </div>
                </div>
            </div>
        )
    }
}

export default GeneratePinModal
