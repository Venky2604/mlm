import React, { Component } from 'react'
import ModalClose from "../closemodal";
import {connect} from "react-redux"
import { withRouter } from "react-router-dom"


class AssignPinModal extends Component {
    render() {
        let { amount , epin } = this.props.selected;
        return (
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLongTitle">E-pin</h5>
                    <ModalClose className="close">
                        <span aria-hidden="true">×</span>
                    </ModalClose>
                </div>
                <div className="modal-body pt0">
                    <br />
                    <div className="row">
                        <div className="col-lg-12" align="center">
                            <p className="epinamount">E-Pin Cost : Rs. {amount}</p>
                            <p className="yourcode">YOUR E-PIN NUMBER</p>
                            <a className="coupon_code"><span className="coupon_icon"><i className="flaticon flaticon-interface-10"></i></span> {epin}
                        </a>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.user.data,
        selected: state.epins.selected[0]
    }
}

export default withRouter(connect(mapStateToProps)(AssignPinModal))