import React from 'react'
import Header from './Header/header';
import Footer from './footer';
import MainLayout from './Mainlayout/mainlayout';
import CenterModal from './centermodal';

const Layout = ({children}) => {
  return (
    <div className="m-grid m-grid--hor m-grid--root m-page">
      <Header />
      <MainLayout>
        {children}
      </MainLayout>
      <Footer />
      <CenterModal />
    </div>
  )
}

export default Layout
