import React, {Component} from 'react';
import userprofilebg from '../../assets/app/media/img/misc/user_profile_bg.jpg';
import userpng from '../../assets/images/user.png';
import { connect } from "react-redux";
import ClickOutWrapper from '../clickoutwrapper';
import { withRouter } from "react-router-dom";
import axios from "axios";
import { urls, clearUser } from "../../actions/common";

class Header extends Component {

  constructor(props){
    super(props);
    this.state =  {
      dropdown: false
    };
    this.toggleDropdown = this.toggleDropdown.bind(this);
    this.closeDropdown = this.closeDropdown.bind(this);
    this.logout = this.logout.bind(this);
  }

  toggleDropdown(){
    this.setState({ dropdown: true });
  }

  closeDropdown(){
    this.setState({dropdown: false});
  }

  async logout(){
    let {data, dispatch, history} = this.props;
    try {
        let res = await axios({
            url: urls.logout,
            method: "delete",
            headers: {
                "Authorization": `Bearer ${data.bearer_token}`
            }
        });
        console.log(res);   
    } catch(error) {
        console.log(error);
    }
    sessionStorage.clear();
    dispatch(clearUser());
    history.replace("/");
  }

  render(){
    return (
      <header id="m_header" className="m-grid__item  m-header ">
        <div className="m-container m-container--fluid m-container--full-height">
          <div className="m-stack m-stack--ver m-stack--desktop">
            <div className="m-stack__item m-brand  m-brand--skin-light ">
              <div className="m-stack m-stack--ver m-stack--general">
                <div className="m-stack__item m-stack__item--middle m-brand__logo">
                  <a className="m-brand__logo-wrapper logoanchor">
                    <h2 className="logotext">KPS&nbsp;TOURS</h2>
                  </a>
                </div>
                <div className="m-stack__item m-stack__item--middle m-brand__tools">

                  <a id="m_aside_left_minimize_toggle" className="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block">
                    <span></span>
                  </a>

                  <a id="m_aside_left_offcanvas_toggle" className="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                    <span></span>
                  </a>

                  <a id="m_aside_header_topbar_mobile_toggle" className="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                    <i className="flaticon-more"></i>
                  </a>

                </div>
              </div>
            </div>

            <div className="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
              <div id="m_header_topbar" className="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
                <div className="flex-center headertag"><p>Sri Mani Muthappan Iya Thunai</p></div>
                <div className="m-stack__item m-topbar__nav-wrapper">
                  <ul className="m-topbar__nav m-nav m-nav--inline">
                    <li className={"m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light" + (this.state.dropdown ? " dropdown--open" : "" )}
                      m-dropdown-toggle="click">
                      <a className="m-nav__link m-dropdown__toggle" onClick={this.toggleDropdown}>

                        <span className="m-topbar__userpic">
                          <img src={userpng} className="m--img-rounded m--marginless" alt="" />
                        </span>
                        <span className="m-topbar__username m--hide">{this.props.user.user.user_profile.name}</span>
                      </a>
                      <ClickOutWrapper clickFunc={this.closeDropdown}>
                      <div className="m-dropdown__wrapper">
                        <span className="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                        <div className="m-dropdown__inner">
                          <div className="m-dropdown__header m--align-center" style={{"background" : `url(${userprofilebg})`, backgroundSize: "cover"}}>
                            <div className="m-card-user m-card-user--skin-dark">
                              <div className="m-card-user__pic">
                                <img src={userpng} className="m--img-rounded m--marginless" alt="" />
                              </div>
                              <div className="m-card-user__details">
                                <span className="m-card-user__name m--font-weight-500">{this.props.user.user.username}</span>
                                <a className="m-card-user__email m--font-weight-300 m-link">{this.props.user.user.user_profile.name}</a>
                              </div>
                            </div>
                          </div>
                          <div className="m-dropdown__body">
                            <div className="m-dropdown__content">
                              <ul className="m-nav m-nav--skin-light" align="center">
                                <li className="m-nav__item">
                                  {/* <a className="btn m-btn--pill mr-3 btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder theme-color-text">Edit Password</a> */}
                                  <a onClick={this.logout} className="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder theme-color-text">Logout</a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      </ClickOutWrapper>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.user.data
  }
}

export default withRouter(connect(mapStateToProps)(Header))
