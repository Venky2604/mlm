import React, { Component } from 'react';
import Listing from './listing';
import { connect } from "react-redux";

const ListingHOC = (ListItem, reducer) => {
    class StoreConnectedList extends Component {
        render() {
            return (
                <Listing connect={this.props} Component={ListItem} />
            )
        }
    }   
    return connect((state) => state[reducer])(StoreConnectedList)
} 

export default ListingHOC;