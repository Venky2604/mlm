import React from 'react'
import SideMenu from './sidemenu';
import { connect } from "react-redux";

class MainLayout extends React.Component{
  render(){
    return (
      <div className="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <button className="m-aside-left-close  m-aside-left-close--skin-dark" id="m_aside_left_close_btn">
          <i className="la la-close"></i>
        </button>
        <div id="m_aside_left" className="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
            <SideMenu role={this.props.role}/>
        </div>
        {this.props.children}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    role: state.user.data.user.role_id
  }
}

export default connect(mapStateToProps)(MainLayout)
