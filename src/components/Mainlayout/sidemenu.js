import React from 'react';
import MenuItem from './menuitem';
import Logout from './logout';

const routesSuperAdmin = [
	{name: "Dashboard", route: "/", exact: true},
	{name: "E-Pins", route: "/epins"},
	{name: "Customer", route: "/customer-management"},
	{name: "Genealogy", route: "/tree"},
	{name: "Levels", route: "/levels"},
	{name: "Tours & Salary", route: "/tours"},
	{name: "Auto Filling", route: "/auto-filling"},
	{name: "Auto Filling Payout", route: "/payout"},
	{name: "Admin/Staff Management", route: "/admin-management"},
	// {name: "Change Password", route: "/change-password"},
];

const routesAdmin = [
	{name: "Dashboard", route: "/", exact: true},
	{name: "E-Pins", route: "/epins"},
	{name: "Customer", route: "/customer-management"},
	{name: "Genealogy", route: "/tree"},
	{name: "Levels", route: "/levels"},
	{name: "Tours & Salary", route: "/tours"},
	{name: "Auto Filling", route: "/auto-filling"},
	{name: "Auto Filling Payout", route: "/payout"},
	// {name: "Change Password", route: "/change-password"},
];

const routesStaff = [
	{name: "Customer", route: "/customer-management"},
	{name: "Genealogy", route: "/tree"},
	{name: "Levels", route: "/levels"},
	{name: "Tours & Salary", route: "/tours"},
	{name: "Auto Filling", route: "/auto-filling"},
	// {name: "Change Password", route: "/change-password"},
];

const routesUser = [
	{name: "Dashboard", route: "/", exact: true},
	{name: "Genealogy", route: "/tree"},
	// {name: "Levels", route: "/levels"},
	// {name: "Tours & Salary", route: "/tours"},
	{name: "Payment", route: "/payout"},
	// {name: "Change Password", route: "/change-password"},
];

const routeMap = {
	1: routesSuperAdmin,
	2: routesAdmin,
	3: routesStaff,
	5: routesUser
};

const SideMenu = ({role}) => {
	return (
		<div id="m_ver_menu" className="m-aside-menu m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark m-scroller ps ps--active-y" style={{position: "relative"}}>
			<ul className="m-menu__nav  m-menu__nav--dropdown-submenu-arrow mt20">
				{
					routeMap[role].map((route, index) => 
						<MenuItem exact={route.exact ? true : false} name={route.name} route={route.route} key={"menu-" + index}/>
					)
				}
				<Logout />
			</ul>
		</div>
	)
}

export default SideMenu