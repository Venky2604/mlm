import React, { Component } from 'react'
import { connect } from 'react-redux';
import axios from "axios";
import { urls, clearUser, openModal } from "../../actions/common";
import { withRouter } from "react-router-dom"

class Logout extends Component {

    constructor(props){
        super(props);
        this.logout = this.logout.bind(this);
    }

    async logout(){
        let {data, dispatch, history} = this.props;
        try {
            let res = await axios({
                url: urls.logout,
                method: "delete",
                headers: {
                    "Authorization": `Bearer ${data.bearer_token}`
                }
            });
            console.log(res);
        } catch(error) {
            console.log(error);
        }
        sessionStorage.clear();
        dispatch(clearUser());
        history.replace("/");
    }

    render() {
        return (
            <React.Fragment>
                <li className="m-menu__item">
                    <a className="m-menu__link" onClick={() => this.props.dispatch(openModal({
                        center: true, modalName: "CHANGE_PWD", head: "Change Password"
                    }))}>
                        <span className="m-menu__link-title">
                            <span className="m-menu__link-wrap">
                                <span className="m-menu__link-text">Change Password</span>
                            </span>
                        </span>
                    </a>
                </li>
                <li className="m-menu__item">
                    <a className="m-menu__link" onClick={this.logout}>
                        <span className="m-menu__link-title">
                            <span className="m-menu__link-wrap">
                                <span className="m-menu__link-text">Logout</span>
                            </span>
                        </span>
                    </a>
                </li>
            </React.Fragment>
        )
    }
}

export default withRouter(connect( (state) => state.user )(Logout)) 