import React from 'react';
import { NavLink } from 'react-router-dom';

const MenuItem = ({name, route, exact }) => {
  return (
    <li className="m-menu__item">
      <NavLink exact={exact} to={route} activeClassName="menu_active" className="m-menu__link">
        <span className="m-menu__link-title">
          <span className="m-menu__link-wrap">
            <span className="m-menu__link-text">{name}</span>
          </span>
        </span>
      </NavLink>
    </li>
  )
}

export default MenuItem
