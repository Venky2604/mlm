import React, { Component } from 'react';
import Layout from './layout';
import { Switch, Route } from "react-router-dom";
import EpinsPage from './epins/epinspage';
import DashboardPage from './Dashboard/dashboardpage';
import UsersPage from './users/userspage';
import Tree from './Tree/treepage';
import LevelsPage from './reports/levelspage';
import ToursPage from './reports/tourspage';
import CustomersPage from './users/customerspage';
import LevelDetail from './reports/leveldetail';
import AutoFillingPage from './reports/autofillingpage';
import AutoDetail from './reports/autodetail';
import PayoutPage from './reports/payoutpage';

class Routes extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route exact={true} path="/" component={DashboardPage} />
          <Route path="/level-detail/:id" component={LevelDetail} />
          <Route path="/auto-filling-achievers/:id" component={AutoDetail} />
          <Route path="/customer-management" component={CustomersPage} />
          <Route path="/admin-management" component={UsersPage} />
          <Route path="/auto-filling" component={AutoFillingPage} />
          <Route path="/epins" component={EpinsPage} />
          <Route path="/tree" component={Tree} />
          <Route path="/levels" component={LevelsPage} />
          <Route path="/payout" component={PayoutPage} />
          <Route path="/tours" component={ToursPage} />
        </Switch>
      </Layout>
    );
  }
}

export default Routes;