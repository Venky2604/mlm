import React, { Component } from 'react'
import { connect } from "react-redux"
import { Route, Redirect } from "react-router-dom"

class ProtectedRoute extends Component {
    render() {
        let { data, path, component, exact} = this.props;
        return (
            data.bearer_token ? 
            <Route path={path} exact={exact} component={component}/> :
            <Redirect to= {"/login"} />
        )
    }
}

export default connect( (state) => state.user  )(ProtectedRoute)