import React, { Component } from 'react'
import {connect} from 'react-redux'
import Portal from './portal';
// import { closeModal } from "../actions/common"

class Modal extends Component {
    render() {
        return (
            this.props.open &&
            <Portal>
                {this.props.children}
                <div className="m-quick-sidebar-overlay fade show"></div>
            </Portal>
        )
    }
}

export default connect((state) => state.modalReducer)(Modal)