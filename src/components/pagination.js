import React from 'react'
import { Link } from "react-router-dom"

const Pagination = ({ current , last , pagesCount = 5, location}) => {
    return (
        <ul className="pagination floatright">
            { current !== 1 && <li className="paginate_button page-item previous" id="m_table_1_previous"><Link to={`${location}&page=${current - 1}`} aria-controls="m_table_1" data-dt-idx="0" tabIndex="0" className="page-link"><i className="la la-angle-left"></i></Link></li>}
            {
                paginationNumbers(current, last, pagesCount).map((page) => 
                    <li key={`page-${page}`} className={`paginate_button page-item ${current === page ? "active" : ''}`}><Link  to={`${location}&page=${page}`} className={`page-link`}>{page}</Link></li>
                )
            }
            {current !== last && <li className="paginate_button page-item next" id="m_table_1_next"><Link  to={`${location}&page=${current + 1}`} className="page-link"><i className="la la-angle-right"></i></Link></li>}
        </ul>
    )
}

const paginationNumbers = (current, last, pagesCount) => {
    let inc = Math.floor(pagesCount/2);
    let half = current + inc;
    let pages = [];
    if( half <= pagesCount ){
        for(let i = 1; i <= pagesCount && i <= last ; i++){
            pages.push(i);
        }
    } else if( half >= last ){
        for(let i = (last - pagesCount > 1? last - pagesCount : 1); i <= last ; i++){
            pages.push(i);
        }
    } else {
        for(let i = (current - inc) ; i <= (current + inc); i++){
            pages.push(i);
        }
    }
    return pages;
}

export default Pagination
