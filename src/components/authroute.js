import React, { Component } from 'react'
import { connect } from "react-redux"
import { Route, Redirect } from "react-router-dom"

class AuthRoute extends Component {
    render() {
        let { data, path, component, exact} = this.props;
        return (
            data.bearer_token ? 
            <Redirect to= {"/mlm"} /> :
            <Route path={path} exact={exact} component={component}/>
        )
    }
}

export default connect( (state) => state.user  )(AuthRoute)