import React, { Component } from 'react'
import { connect } from "react-redux"
import plusimg from "../../assets/images/plus.png";
import { openModal } from '../../actions/common';

class AddChild extends Component {
    render() {
        let { user_id, spName, spId } = this.props;
        console.log(this.props);
        return (
            <li>
                <a onClick={()=> this.props.dispatch(openModal({center: true, modalName: "ADD_USER_MODAL", head: "Add User", user_id, spName, spId }))}>
                    <img src={plusimg} className="geneimg" alt=""/>
                    <p className="geneuser treeAddColor">Add User</p>
                </a>
            </li>
        )
    }
}

const mapStateToProps = (state) => {
    return { 
        user_id: state.userProfile.data.id
    }
}

export default connect(mapStateToProps)(AddChild)