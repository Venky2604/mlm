import React from 'react'
import { dateTimeFormat } from "../../actions/common"

const Userdetails = ({user_profile, referrer, phone, created_at, downline_count, auto_filling_count, username}) => {
    return (
        <div className="geneologysection">
            <div className="row">
                <div className="col-lg-3">
                    <p className="geneologytext">Your Name : <br /><span>{user_profile.name}</span></p>
                    <p className="geneologytext">Your ID : <br /><span>{username}</span></p>
                </div>
                <div className="col-lg-3">
                    <p className="geneologytext">Sponsor Name : <br /><span>{referrer ? referrer.referrer.user_profile.name : "Company"}</span></p>
                    <p className="geneologytext">Sponsor ID : <br /><span>{referrer ? referrer.referrer.username : "KPS0000"}</span></p>
                </div>
                <div className="col-lg-3">
                    <p className="geneologytext">Phone No. : <br /><span>{phone}</span></p>
                    <p className="geneologytext">Joined Date : <br /><span>{dateTimeFormat(created_at)}</span></p>
                </div>
                <div className="col-lg-3">
                    <p className="geneologytext">Direct Line  : <br /><span>{downline_count}</span></p>
                    <p className="geneologytext">Autofilling Count : <br /><span>{auto_filling_count}</span></p>
                </div>
            </div>
        </div> 
    )
}

export default Userdetails
