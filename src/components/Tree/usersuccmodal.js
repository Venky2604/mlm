import React, { Component } from 'react'
import ModalClose from "../closemodal";

export default class UserSuccModal extends Component {
    render() {
        return (
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLongTitle">{this.props.head}</h5>
                    <ModalClose className="close">
                        <span aria-hidden="true">×</span>
                    </ModalClose>
                </div>
                <div className="modal-body">
                    <div className="m-login__form m-form">
                        <div className="row">
                            <div className="col-md-6">
                                <h5>Customer ID : {this.props.username}</h5>
                            </div>
                            <div className="col-md-12">
                                <h5>Password : {this.props.pwd}</h5>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        )
    }
}
