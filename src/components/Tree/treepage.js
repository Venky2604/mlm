import React, { Component } from 'react'
import { connect } from 'react-redux';
import { userProfile, formOption, urlParams } from "../../actions/common"
import Userdetails from './userdetails';
import TreeStructure from './tree';

class Tree extends Component {

    componentDidMount(){
        let { user, location, dispatch } = this.props;
        let user_id = urlParams(location.search).user;
        dispatch( userProfile((user_id ? user_id : user.user.role.id > 2 ? user.user.id : 3), user.bearer_token) );
        dispatch(formOption(user.bearer_token));
    }

    componentWillReceiveProps(nextProps){
        if(this.props.location.search !== nextProps.location.search ){
            let { user, location, dispatch } = nextProps;
            let user_id = urlParams(location.search).user;
            dispatch( userProfile((user_id ? user_id : user.user.role.id === 5 ? user.user.id : 3), user.bearer_token) );
        }
    }

    render() {
        let { data, fetching } = this.props.tree;
        return (
            <div className="m-grid__item m-grid__item--fluid m-wrapper">
                <div className="m-content" style={{minHeight: `1000px !important`}}>
                    <div className="m-portlet m-portlet--mobile">
                        <div className="row tableheaderrow">
                            <div className="col-md-4 col-sm-3">
                                <h3 className="m-portlet__head-text tableheading">
                                    Genealogy
                                </h3>
                            </div>
                            <div className="col-md-8 col-sm-9">
                                <div className="input-group">
                                    <input type="text" className="form-control form-control-warning searchinput" placeholder="Search by Name, Mobile Number..." />
                                    <div className="input-group-append">
                                        <button className="btn btn-brand searchbtn" type="button">Go!</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="m-portlet__body pb60">
                            {fetching && <div className="flex-center listingminheight"><div className="listingloader"></div></div>}
                            {data.id ? <Userdetails {...data} /> : null}
                            {data.id ? <TreeStructure {...data} /> : null}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return { 
        user: state.user.data,
        tree: state.userProfile
    }
}

export default connect(mapStateToProps)(Tree)