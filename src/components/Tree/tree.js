import React, { Component } from 'react'
import TreeChildUser from './treechilduser';

export default class TreeStructure extends Component {
    render() {
        let { user_profile, user_referrals, username } = this.props;
        return (
            <div className="tree">
                <ul>
                    <TreeChildUser parent={true} userId={user_profile.user_id} customerId={username} name={user_profile.name} child={user_referrals}/>
                </ul>
            </div>
        )
    }
}
