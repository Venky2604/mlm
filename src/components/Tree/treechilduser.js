import React, {Component} from 'react'
import userimg from "../../assets/images/user.png";
import AddChild from './addchild';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom"

class TreeChildUser extends Component {

    constructor(props){
        super(props);
        this.loadChild = this.loadChild.bind(this);
        // this.loadChildAsyc = this.loadChildAsyc.bind(this);
    }

    loadChild(){
        if(this.props.parent){
            return
        }
        // this.loadChildAsyc()
        this.props.history.push("/tree?user=" + this.props.data.user.id);
    }

    // async loadChildAsyc(){
    //     try{
    //         let res = await  axios.get(`${urls.listUsers}/${this.props.id}`, {
    //             headers: {
    //                 "Accept": "application/json",
    //                 "Authorization": `Bearer ${this.props.token}`
    //             }
    //         });
    //         console.log(res.data.user_referrals)
    //         this.setState({ chilusers: res.data.user_referrals })
    //     } catch(error){
    //         console.log(error);
    //     }
        
    // }

    render(){ 
        let {name, child , parent,  data, customerId, spName, spId, user, userId} = this.props;
        return (
            <li>
                <a className={parent ? "" : "popover__wrapper"} onClick={this.loadChild}>
                    <img src={userimg} className="geneimg" alt=""/>
                    <p className="geneuser treeUserColor">{name}<br /> {customerId}</p>
                    {!parent &&<div className="push popover__content">
                        <p className="geneologytext">Direct Line : <span>{data.user.downline_count}</span></p>
                        <p className="geneologytext">Autofilling Count : <span>{data.user.auto_filling_count}</span></p>
                        <p className="geneologytext">Phone : <span>{data.user.phone}</span></p>
                        <p className="geneologytext">Sponsor Name : <span>{spName ? spName : "Company"}</span></p>
                        <p className="geneologytext">Sponsor ID : <span>{spId ? spId : "KPS0000"}</span></p>
                        
                        
                    </div>}
                </a>
                {
                    parent ? 
                    <ul>
                        {
                            child.map(item => <TreeChildUser customerId={item.user.username} name={item.user.user_profile.name} spName={name} spId={customerId}  data={item} history={this.props.history}  key={"parent" + item.user.id}/>)
                        }
                        {
                            user.user.role_id === 5  && user.user.id !== userId ? child.length === 0 ? <p className="errtxt">Last User !</p> : null :
                            Array.apply(null, {length: 5- child.length}).map(Function.call, Number).map((x) => <AddChild spName={name} spId={customerId} key={"add" + x} />)
                        }
                    </ul>
                    : null
                }
            </li>
        )
    }
}

const mapStateToProps = (state) => {
    let { user } = state;
    return {
        user: user.data
    }
}

export default withRouter(connect(mapStateToProps)(TreeChildUser))
