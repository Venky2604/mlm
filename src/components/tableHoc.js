import React, { Component } from 'react'
import {connect} from "react-redux"

export const tableHOC = (TableItem, reducer, headings = []) => {

    class TableConnected extends Component{
        render() {
            let { fetching, error, data } = this.props;
            if(fetching || error || data.length === 0){
                return <TableItem />
            } else {
                return (
                    <TableContainer headings={headings}>
                        <TableItem />
                    </TableContainer>
                )
            }
        }
    }
    return connect((state) => state[reducer])(TableConnected)
} 

export const TableContainer = ({headings , children}) => (
    <table className="table table-striped- table-bordered table-hover table-responsive subadmintable">
        <thead>
            <tr>
                {
                    headings.map((head, index) => <th key={"head" + index}>{head}</th>)
                }
            </tr>
        </thead>
        <tbody>
            {children}
        </tbody>
    </table>
)