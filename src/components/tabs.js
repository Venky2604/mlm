import React, { Component } from 'react'

export default class Tabs extends Component {

    state = {
        tab: 0
    };
    
    changeTab(tab){
        this.setState({tab})
    }

    render() {
        return (
            <div className="m-portlet__body pb60">
                <div className="row">
                    <div className="col-md-8">
                        <ul className="nav nav-tabs  m-tabs-line" role="tablist">
                            {
                                this.props.tabs.map((tab, index) => 
                                    <li key={tab.name + "tab" + index} className="nav-item m-tabs__item">
                                        <a onClick={() => this.changeTab(index)} className={`nav-link m-tabs__link${this.state.tab === index ? " active" : ''}`} data-toggle="tab" role="tab" key={"tab-link-" + index}>{tab.name}</a>
                                    </li>
                                )
                            }
                        </ul>
                    </div>
                    {this.props.actions}
                </div>
                <div className="tab-content">
                    <div className="tab-pane active" id="m_tabs_1_1" role="tabpanel">
                        <div className="row">
                            {
                                this.props.tabs[this.state.tab].tabComponent
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}