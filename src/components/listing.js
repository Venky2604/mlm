import React from 'react'

const Listing = ({Component, connect}) => {
    let { fetching, data, error, dispatch, current_page } = connect; 
    if (error) return null;
    if (fetching) return <div className="flex-center listingminheight"><div className="listingloader"></div></div>;
    if (data.length === 0) return <div className="flex-center listingminheight errtxt">No data found</div>;
    return data.map((item, index) => (<Component slno={index + 1 +  (current_page ? ((current_page -1) * 15) : 0 )} dispatch={dispatch} {...item} key={item.id ? item.id : "listing-null-" + index} />) );
}

export default Listing
