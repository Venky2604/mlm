import React, { Component } from 'react'
import ListingHOC from '../listinghoc';
import { tableHOC } from '../tableHoc';
import { Link } from "react-router-dom";

class DetailItem extends Component {
    render() {
        let {slno, phone, user_profile, referrer, username, month , year} = this.props;
        return (
            <tr>
                <td>{slno }</td>
                <td><Link to={`/tree?user=${user_profile.user_id}`}>{username}</Link></td>
                <td><Link to={`/tree?user=${user_profile.user_id}`}>{user_profile.name}</Link></td>
                <td>{phone}</td>
                <td>{referrer ? <Link to={`/tree?user=${referrer.referrer.user_profile.user_id}`}>{referrer.referrer.user_profile.name}</Link> : 'Company'}</td>
                <td>{referrer ? <Link to={`/tree?user=${referrer.referrer.user_profile.user_id}`}>{referrer.referrer.username}</Link> : 'kps10000'}</td>
                <td>{`${month} ${year}`}</td>
            </tr>
        )
    }
}

const heading = ["S.No.", "Customer ID", "Name", "Mobile No.", "Sponsor", "Sponsor ID","Eligible Month"];

export const SalaryItem = tableHOC(ListingHOC(DetailItem, "listSalaryDetail"), "listSalaryDetail", heading);
export const TourItem = tableHOC(ListingHOC(DetailItem, "listTourDetail"), "listTourDetail", heading);
