import React, { Component } from 'react'
import ListingHOC from '../listinghoc';
import { tableHOC } from '../tableHoc';
import { Link, withRouter } from "react-router-dom";

class LevelDetailItem extends Component {
    render() {
        let {slno, phone, user_profile, referrer, username, month , year} = this.props;
        return (
            <tr>
                <td>{slno }</td>
                <td><Link to={`/tree?user=${user_profile.user_id}`}>{username}</Link></td>
                <td><Link to={`/tree?user=${user_profile.user_id}`}>{user_profile.name}</Link></td>
                <td>{phone}</td>
                <td>{referrer ? <Link to={`/tree?user=${referrer.referrer.user_profile.user_id}`}>{referrer.referrer.user_profile.name}</Link> : 'Company'}</td>
                <td>{referrer ? <Link to={`/tree?user=${referrer.referrer.user_profile.user_id}`}>{referrer.referrer.username}</Link> : 'kps10000'}</td>
                <LevelTd />
                <td>{`${month} ${year}`}</td>
            </tr>
        )
    }
}

const levelAchieved = ({match}) => (<td>{match.params.id}</td>);

const LevelTd = withRouter(levelAchieved);

const heading = ["S.No.", "Customer ID", "Customer Name", "Mobile No.", "Sponsor", "Sponsor ID", "Level Achieved", "Month"];

export default tableHOC(ListingHOC(LevelDetailItem, "listLevelDetail"), "listLevelDetail", heading)
