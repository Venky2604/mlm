import React, { Component } from 'react'
import ListingHOC from '../listinghoc';
import { tableHOC } from '../tableHoc';
import { Link } from "react-router-dom";
import { openModal, paySelect, loadLocalState } from '../../actions/common';


const role = sessionStorage.mlmuser ? loadLocalState().user.role.id : '';

class PayoutItem extends Component {

    selectUser(){
        let { id, dispatch} = this.props;
        dispatch(paySelect(id));
        dispatch(openModal({center: true, modalName: "PAY_MODAL", head: "Payment Release"}))
    }

    render() {
        let {slno, user_profile, plan, username, month, year, payout } = this.props;
        return (
            <tr>
                <td>{slno }</td>
                <td>{month} - {year}</td>
                <td><Link to={`/tree?user=${user_profile.user_id}`}>{username}</Link></td>
                <td><Link to={`/tree?user=${user_profile.user_id}`}>{user_profile.name}</Link></td>
                <td>{plan.size}</td>
                <td>{ cur(plan.pay_out) }</td>
                <td>{cur( ( plan.tax/100) * plan.pay_out )}</td>
                <td>{cur(plan.net_pay)}</td>
                <td>
                    {   role === 5 ? 
                        payout ? 
                        <button className="btn btn-sm btn-success">Paid</button> : 
                        <button className="btn btn-sm btn-warning" >Pending</button> 
                        : 
                        payout ? 
                        <button className="btn btn-sm btn-success">Paid</button> : 
                        <button className="btn btn-sm btn-primary" onClick={() => this.selectUser()}>Pay</button> 
                    }
                </td>
            </tr>
        )
    }
}

const cur = (val) => Number(val).toLocaleString('en-IN', { style: 'currency', currency: 'INR'})

const heading = ["S.No.", "Month", "Customer ID", "Customer Name", "Reach Team", "Eligible Payment", "Tax+Charges(15%)", "Net Pay", "Actions"];

export default tableHOC(ListingHOC(PayoutItem, "listPayoutDetail"), "listPayoutDetail", heading)
