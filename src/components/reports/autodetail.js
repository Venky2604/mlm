import React, { Component } from 'react'
import { connect } from 'react-redux'
import { listAutoDetail, autoDetailClear, urls } from '../../actions/common';
import AutoDetailItem from './autodetailitem';

class LevelDetail extends Component {

    componentDidMount(){
        this.props.dispatch(listAutoDetail(this.props.match.params.id, this.props.user.bearer_token));
    }

    render() {
        return (
            <div className="m-grid__item m-grid__item--fluid m-wrapper">
                <div className="m-content">
                    <div className="m-portlet m-portlet--mobile">
                        <div className="row tableheaderrow">
                            <div className="col-md-10">
                                <h3 className="m-portlet__head-text tableheading">
                                    Auto-Filling Plan { this.props.match.params.id } Achievers
                                </h3>
                            </div>
                            <div className="col-md-2">
                                <a href={`${urls.exportAuto}/${this.props.match.params.id}/export`} className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright width100">Export</a>
                            </div>
                        </div>
                        <div className="m-portlet__body pb60">
                            <div className="tab-content">
                                <AutoDetailItem />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentWillUnmount(){
        this.props.dispatch(autoDetailClear());
    }

}

const mapStateToProps = (state) => ({
  user: state.user.data
})

export default connect(mapStateToProps)(LevelDetail)
