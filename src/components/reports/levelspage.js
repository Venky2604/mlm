import React, { Component } from 'react'
import { connect } from 'react-redux'
import { listLevelsMonth, urlParams, clearLevel } from '../../actions/common';
import LevelItem from './leveltem';

class LevelsPage extends Component {

    componentDidMount(){
        this.props.dispatch(listLevelsMonth(urlParams(this.props.location.search).tab, this.props.user.bearer_token));
    }

    changeTab(tab) {
        let { history, levels } = this.props;
        if(levels.fetching){
            return;
        }
        history.replace(`/levels?tab=${tab}`);
    }

    componentWillReceiveProps(nextProps){
        let { location, user, dispatch } = nextProps
        if(nextProps.location.search !== this.props.location.search ){
            dispatch(listLevelsMonth(urlParams(location.search).tab, user.bearer_token));
        }
    }

    render() {
        let params = urlParams(this.props.location.search);
        let tab = params.tab ? parseInt(params.tab, 10) : 1; 
        let { fetching } = this.props.levels;
        return (
            <div className="m-grid__item m-grid__item--fluid m-wrapper">
                <div className="m-content">
                    <div className="m-portlet m-portlet--mobile">
                        <div className="row tableheaderrow">
                            <div className="col-md-4">
                                <h3 className="m-portlet__head-text tableheading">
                                    Level Report - <span className="theme-color-text">{new Date().toLocaleDateString("en-IN", { year: 'numeric', month: 'long'})}</span>
                                </h3>
                            </div>
                            <div className="col-md-4">
                            
                            </div>
                            <div className="col-md-2">
                                {/* <a className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright width100">Detail Levels</a> */}
                            </div>
                            <div className="col-md-2">
                                {/* <a className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright width100">Overall</a> */}
                            </div>
                        </div>
                        <div className="m-portlet__body pb60">
                            <div className="row">
                                <div className="col-lg-6">
                                    <ul className="nav nav-tabs  m-tabs-line" role="tablist">
                                        <li className="nav-item m-tabs__item">
                                            <a onClick={() => this.changeTab(1)} className={`nav-link m-tabs__link ${tab === 1 ? "active" : ""}  ${fetching ? "disabledpointer" : ''}`} >Current Month</a>
                                        </li>
                                        <li className="nav-item m-tabs__item">
                                            <a onClick={() => this.changeTab(2)} className={`nav-link m-tabs__link ${tab === 2 ? "active" : ""} ${fetching ? "disabledpointer" : ''}`}>Overall</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className="tab-content">
                                <LevelItem />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentWillUnmount(){
        this.props.dispatch(clearLevel());
    }
}

const mapStateToProps = (state) => ({
  user: state.user.data,
  levels: state.listLevels
})

export default connect(mapStateToProps)(LevelsPage)
