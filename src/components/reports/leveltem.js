import React, { Component } from 'react'
import ListingHOC from '../listinghoc';
import { tableHOC } from '../tableHoc';
import { Link } from "react-router-dom";

class LevelItem extends Component {
    render() {
        let {slno, level, count} = this.props;
        return (
            <tr>
                <td>{slno}</td>
                <td>{`Level-${level}`}</td>
                <td><b>{count}</b></td>
                <td>{ count > 0 ? <Link to={"/level-detail/" + level} className="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill infobtn"><i className="la la-info"></i></Link> : " - "}</td>            
            </tr>
        )
    }
}

const heading = ["S.No.", "Level", "Completed", "View Level"];

export default tableHOC(ListingHOC(LevelItem, "listLevels"), "listLevels", heading)
