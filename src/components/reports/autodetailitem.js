import React, { Component } from 'react'
import ListingHOC from '../listinghoc';
import { tableHOC } from '../tableHoc';
import { Link } from "react-router-dom";

class AutoDetailItem extends Component {
    render() {
        let {slno, phone, user_profile, referrer, username, month, year, plan} = this.props;        
        return (
            <tr>
                <td>{slno }</td>
                <td><Link to={`/tree?user=${user_profile.user_id}`}>{username}</Link></td>
                <td><Link to={`/tree?user=${user_profile.user_id}`}>{user_profile.name}</Link></td>
                <td>{phone}</td>
                <td>{referrer ? <Link to={`/tree?user=${referrer.referrer.user_profile.user_id}`}>{referrer.referrer.user_profile.name}</Link> : 'Company'}</td>
                <td>{referrer ? <Link to={`/tree?user=${referrer.referrer.user_profile.user_id}`}>{referrer.referrer.username}</Link> : 'kps10000'}</td>
                <td>{ plan.size }</td>
                <td>{month} - {year}</td>
            </tr>
        )
    }
}


const heading = ["S.No.", "Customer ID", "Customer Name", "Mobile No.", "Sponsor", "Sponsor ID", "Team Size", "Month"];

export default tableHOC(ListingHOC(AutoDetailItem, "listAutoDetail"), "listAutoDetail", heading)
