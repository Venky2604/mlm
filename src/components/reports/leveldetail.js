import React, { Component } from 'react'
import { connect } from 'react-redux'
import { listLevelsDetail, levelDetailClear } from '../../actions/common';
import LevelDetailItem from './leveldetailitem';

class LevelDetail extends Component {

    componentDidMount(){
        this.props.dispatch(listLevelsDetail(this.props.match.params.id, this.props.user.bearer_token));
    }

    render() {
        return (
            <div className="m-grid__item m-grid__item--fluid m-wrapper">
                <div className="m-content">
                    <div className="m-portlet m-portlet--mobile">
                        <div className="row tableheaderrow">
                            <div className="col-md-4">
                                <h3 className="m-portlet__head-text tableheading">
                                    Level {this.props.match.params.id} Achievers
                                </h3>
                            </div>
                        </div>
                        <div className="m-portlet__body pb60">
                            <div className="tab-content">
                                <LevelDetailItem />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    componentWillUnmount(){
        this.props.dispatch(levelDetailClear());
    }
}

const mapStateToProps = (state) => ({
  user: state.user.data
})

export default connect(mapStateToProps)(LevelDetail)
