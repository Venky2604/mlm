import React, { Component } from 'react'
import { connect } from 'react-redux'
import Tabs from '../tabs';
import { TourItem, SalaryItem } from './touritem';
import { listTourDetail, listSalaryDetail } from '../../actions/common';

class ToursPage extends Component {

    componentDidMount(){
        let { dispatch, user } = this.props;
        let token = user.bearer_token;
        dispatch(listTourDetail(token));
        dispatch(listSalaryDetail(token));
    }

    render() {
        return (
            <div className="m-grid__item m-grid__item--fluid m-wrapper">
                <div className="m-content">
                    <div className="m-portlet m-portlet--mobile">
                        <div className="row tableheaderrow">
                            <div className="col-md-4">
                                <h3 className="m-portlet__head-text tableheading">
                                    Tours and Salary
                                </h3>
                            </div>
                            <div className="col-md-4">
                            
                            </div>
                            <div className="col-md-2">
                                {/* <a className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright width100">Detail Levels</a> */}
                            </div>
                            <div className="col-md-2">
                                {/* <a className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright width100">Overall</a> */}
                            </div>
                        </div>
                        <Tabs 
                            tabs={[
                                {name: "Tours", tabComponent: <TourItem />},
                                {name: "Salary", tabComponent: <SalaryItem />}
                            ]}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
  user: state.user.data
})

export default connect(mapStateToProps)(ToursPage)
