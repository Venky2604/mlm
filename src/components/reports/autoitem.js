import React, { Component } from 'react'
import ListingHOC from '../listinghoc';
import { tableHOC } from '../tableHoc';
import { Link } from "react-router-dom";

class AutoItem extends Component {
    render() {
        let {slno, size, pay_out, completed_count, offer, id} = this.props;
        return (
            <tr>
                <td>{slno}</td>
                <td>{size}</td>
                <td>{pay_out}</td>
                <td>{offer ? offer : ' - '}</td>
                <td>{completed_count}</td>
                <td>{ completed_count > 0 ? <Link to={"/auto-filling-achievers/" + id} className="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill infobtn"><i className="la la-info"></i></Link> : " - "}</td>            
            </tr>
        )
    }
}

const heading = ["S.No.", "Team Size", "Eligible Amount" , "Offer" , "Achievers", "View Achievers"];

export default tableHOC(ListingHOC(AutoItem, "listAuto"), "listAuto", heading)
