import React, { Component } from 'react'
import MInput from '../form/minput';
import { connect } from 'react-redux';
import ModalClose from "../closemodal";
import { formstate, closeModal, urls, ajaxerrmsg, payOutList } from '../../actions/common';
import axios from "axios";

const inputs = [
    {name: "bank_name", type: "text", label: "Bank Name"},
    {name: "cheque_id", type: "text", label: "Cheque No."},
    {name: "cheque_date", type: "text", label: "Cheque Date"},
];


class PayModal extends Component {

    constructor(props){
        super(props);
        this.state = {
            ...formstate(inputs),
            "show": 0,
            "disabled": true,
            "error": false,
            "fetching": false
        };
        this.handleChange = this.handleChange.bind(this); 
        this.radioChange = this.radioChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this);  
        this.submitForm = this.submitForm.bind(this);  
        this.cheque = this.cheque.bind(this);  
        this.cash = this.cash.bind(this);  
    }

    handleChange(e){
        let {name, value} = e.target;
        this.setState({[name]: value, [name + "_err"]: ''})
    }

    radioChange(e){
        let {value} = e.target;
        this.setState({show: parseInt(value, 10), disabled: false})
    }

    handleSubmit(e){
        e.preventDefault();
        let { bank_name, cheque_id, cheque_date, show  } = this.state;
        let { payselected } = this.props;
        show === 0 ?
        this.cash({is_cheque: false, user_id: payselected.id, month: "08", year: "2018"}) :
        this.cheque({
            bank_name, cheque_id, cheque_date, is_cheque: true, user_id: payselected.id,
            month: "08", year: "2018"
        })
    }

    cheque(data){
        if(!data.bank_name){
            this.setState({"bank_name_err": "Bank name is required"});
            return;
        }
        if(!data.cheque_id){
            this.setState({"cheque_id_err": "Cheque No. is required"});
            return;
        }
        if(!data.cheque_date){
            this.setState({"cheque_date_err": "Cheque Date is required"});
            return;
        }
        this.submitForm(data);
    }

    cash(data){
        this.submitForm(data);
    }

    async submitForm(data){
        this.setState({fetching: true});
        try {
            let {user, dispatch} = this.props;
            let res = await axios({
                url: urls.payoutList,
                method: "post",
                headers: {
                    "Content-Type" : "application/json",
                    "Accept": "application/json",
                    "Authorization": `Bearer ${user.bearer_token}`
                },
                data: JSON.stringify(data)
            });
            if(res){
                console.log(res);
                this.setState({fetching: false});   
                dispatch(payOutList(user.bearer_token));        
                dispatch(closeModal());            
            }
        } catch(error){
            this.setState({fetching: false});   
            console.log(error.response);
            this.setState({error: ajaxerrmsg(error.response.data)});            
        }
    }

    render() {
        let state = this.state;
        let { fetching, error, disabled } = state;
        let { user_profile, username, plan } = this.props.payselected;
        return (
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="modal-title" id="exampleModalLongTitle">{this.props.head}</h5>
                    <ModalClose className="close">
                        <span aria-hidden="true">×</span>
                    </ModalClose>
                </div>
                <div className="modal-body">
                    <div className="m-login__form m-form">
                        <div className="row">
                            <div className="col-md-6">
                                <p className="geneologytext">Name : <span>{user_profile.name}</span></p>
                            </div>
                            <div className="col-md-6">
                                <p className="geneologytext">Customer ID : <span>{username}</span></p>
                            </div>
                            <div className="col-md-12">
                                <p className="geneologytext">Net Pay : <span>{cur(plan.net_pay)}</span></p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-6 mb-2">
                                <label>
                                    <input onClick={this.radioChange} className="mr-2" name="selectpay" value="0" type="radio" />
                                    Cash
                                </label>
                            </div>
                            <div className="col-md-6 mb-2">
                                <label>
                                    <input onClick={this.radioChange} className="mr-2" name="selectpay" value="1" type="radio" />
                                    Cheque
                                </label>
                            </div>
                        </div>
                        {this.state.show ?
                        <div className="row mt-3">
                            {
                                inputs.map((ip, index) => 
                                    <MInput 
                                        label={ip.label}
                                        inputprops = {{
                                            type: ip.type,
                                            placeholder: index === 2 ? `yyyy-mm-dd` :`Enter ${ip.label}` ,
                                            name: ip.name,
                                            maxLength: ip.maxLength,
                                            value: state[ip.name]
                                        }}
                                        handleChange={this.handleChange}
                                        error={state[ip.name + "_err"]}
                                        key={`input-${index}-${ip.name}`}
                                        half={ip.full ? false :true}
                                    />
                                )
                            }
                        </div> : null
                        }
                        {error &&<div className={"col-md-12"}><p className="errtxt">{error}</p></div>}
                    </div>
                </div>
                <div className="modal-footer">
                    <ModalClose className="btn btn-secondary">Close</ModalClose>
                    <button disabled={disabled} onClick={this.handleSubmit} type="submit" className="btn btn-primary bgbtn">Pay  {fetching && <div className="sp-circle"></div>}</button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    let { user, listPayoutDetail } = state;
    return {
        user: user.data,
        payselected: listPayoutDetail.selected
    }
}

const cur = (val) => Number(val).toLocaleString('en-IN', { style: 'currency', currency: 'INR'})

export default connect(mapStateToProps)(PayModal)