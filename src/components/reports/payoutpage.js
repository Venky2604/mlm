import React, { Component } from 'react'
import { connect } from 'react-redux'
import PayoutItem from './payoutitem';
import { payOutList, urls } from '../../actions/common';

class LevelDetail extends Component {

    componentDidMount(){
        this.props.dispatch(payOutList(this.props.user.bearer_token, this.props.user.user.role.id === 5));
    }

    render() {
        return (
            <div className="m-grid__item m-grid__item--fluid m-wrapper">
                <div className="m-content">
                    <div className="m-portlet m-portlet--mobile">
                        <div className="row tableheaderrow">
                            <div className="col-md-10">
                                <h3 className="m-portlet__head-text tableheading">
                                    Auto-Filling {this.props.user.user.role.id === 5 ? "Payment" : "Payout"}
                                </h3>
                            </div>
                            <div className="col-md-2">{
                                this.props.user.user.role.id !== 5 &&
                                <a href={urls.exportPayout} className="btn m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air addbtn floatright width100">Export</a>}
                            </div>
                        </div>
                        <div className="m-portlet__body pb60">
                            <div className="tab-content">
                                <PayoutItem />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
  user: state.user.data
})

export default connect(mapStateToProps)(LevelDetail)