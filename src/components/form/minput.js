import React from 'react'
import NumberOnlyInput from './numberonlyinput';
import CustomSelect from './customselect';

const MInput = ({inputprops, error, label, handleChange, required=true , half = false }) => {
    return (
        <div className={"form-group m-form__group mb0 pb0" + (half ? " col-lg-6" :  " col-lg-12")}>
            <label className="labelheading">{label}&nbsp;{required&&<span className="required">*</span>}</label>
            {  inputprops.type === "tel"?
                <NumberOnlyInput 
                    className={`form-control m-input ${error ? "errorip" : ""}`} 
                    onChange={handleChange} 
                    {...inputprops} 
                /> :
                inputprops.type === "select" ?
                    <CustomSelect
                        name={inputprops.name}
                        title={label}
                        selectedvalue={inputprops.selectedvalue ? inputprops.selectedvalue : {}}
                        options={inputprops.options}
                        optionName={inputprops.optionname} 
                        optionValue={inputprops.optionvalue} 
                        optionSearch={true}
                        onChange={(value) => handleChange(value, inputprops.name)}
                    />
                :<input className={`form-control m-input ${error ? "errorip" : ""}`} {...inputprops} onChange={handleChange}/> }
            {error && <p className="errtxt">{error}</p>}
        </div>
    )
}

export default MInput
