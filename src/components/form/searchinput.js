import React from 'react';

const SearchInput = ({inputProps, searchClick}) => (
    <div className="relative">
        <input className="form-control searchinput" type="text" {...inputProps} />
    </div>
);

export default SearchInput;