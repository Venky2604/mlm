import React, { Component } from 'react'

class NumberOnlyInput extends Component {

    preventChar(event){
        if(!(event.charCode >= 48 && event.charCode <= 57)){
            event.preventDefault();
            return;
        }
    }

    render() {
        return (
            <input type="tel"  onKeyPress={(e) => this.preventChar(e)} {...this.props}/>
        )
    }
}

export default NumberOnlyInput;
