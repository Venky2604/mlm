import React, { Component } from 'react'
import SearchInput from './searchinput';
import ClickOutWrapper from '../clickoutwrapper';

class CustomSelect extends Component{

    constructor(props){
        super(props);
        this.state = {
            selected: props.selectedvalue ? Object.keys(props.selectedvalue).length ? props.selectedvalue : null : null,
            open: false,
            filter: ''
        };
        this.selectOption = this.selectOption.bind(this);
        this.dropdownToggle = this.dropdownToggle.bind(this);
        this.filterOptions = this.filterOptions.bind(this);
        this.close = this.close.bind(this);
    }

    selectOption(selected, open){
        let {onChange, optionValue} = this.props;
        this.setState({selected, open});
        if(onChange){
            onChange(selected[optionValue]);
        }
    }

    dropdownToggle(){
        this.setState(function(prevState){
            return {
                open: !prevState.open
            }
        })
    }

    close(){
        this.setState({open: false});
    }

    filterOptions(e){
        let filter = e.target.value;
        this.setState({ filter })
    }


    render(){
        let {options, title, optionSearch, optionName, optionValue} = this.props;
        let {selected, open, filter} = this.state;
        let filterOptions = this.filterOptions;
        return(
            <ClickOutWrapper clickFunc={this.close}>
            <div 
                className={"dropup customSelect" + (open ? " show" : "")}
            >          
                <button className="form-control border btn btn-lg btn-light" type="button" onClick={this.dropdownToggle}>
                    <div className="flex-center CustomSelectedOptionDiv">
                        {selected !== null ? selected[optionName] : title}
                    </div>
                    <span className="flex-center fa fa-chevron-down f14"></span>
                </button>
                <div 
                    className={"dropdown-menu  customSelectDropdown" + (open ? " show" : "")}
                >   
                    {optionSearch &&
                        <div className="customSelectSearch">
                                <SearchInput 
                                inputProps={{
                                    name: "search", 
                                    value: filter,
                                    placeholder: "Search Options",
                                    onChange: ( (e) =>  filterOptions(e) )
                                }} 
                                searchClick={()=> false}
                            />
                        </div>
                    }
                    <div className="optionsdiv">
                        {
                            options
                                .filter(option => 
                                    option[optionName].toLowerCase().indexOf(filter.toLowerCase()) !== -1
                                )
                                .map((option) => (
                                    <div 
                                        className={`dropdown-item pointer whitespacewrap${selected && selected[optionValue] === option[optionValue] ? ' active' : '' }`} 
                                        onClick={() =>
                                            this.selectOption(option, false)
                                        }
                                        key={option[optionValue] + option[optionName]}
                                    >
                                        {option[optionName]}
                                    </div>
                                ))
                        }
                    </div>
                </div>
            </div>
            </ClickOutWrapper>
        );
    }
} 

export default CustomSelect;
