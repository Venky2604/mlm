import React, { Component } from 'react'
import { pwdvalid, urlParams, resetpwd, saveLocalState, ajaxerrmsg } from "../../actions/common"
import { connect } from "react-redux"

class ResetForm extends Component {

    constructor(props){
        super(props);
        this.state = {
            password: '',
            password_err: false,
            confirm_password: '',
            confirm_password_err: false
        };
        this.handleChange = this.handleChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this); 
    }

    handleChange(e){
        let {name, value} = e.target;
        this.setState({[name]: value, [name + "_err"]: false});
    }

    handleSubmit(e){
        e.preventDefault();
        let {confirm_password, password} = this.state;
        if(!pwdvalid(password)){
            this.setState({password_err: true});
            return;
        }
        if(!pwdvalid(confirm_password)){
            this.setState({confirm_password_err: "Password must atleast 6 characters"});
            return;
        }
        if(password !== confirm_password){
            this.setState({confirm_password_err: "Passwords do not match"});
            return;
        }
        let token = urlParams(this.props.location.search).token;
        console.log(this.props);
        if(token){
            this.props.dispatch(resetpwd({
                password, token
            }))
        } 
    }

    componentWillReceiveProps(nextProps){
        let { data, error, history } = nextProps;
        if(!error && data.bearer_token){
            saveLocalState(data);
            history.replace("/");
        }
    }

    render() {
        let { confirm_password_err, password_err, password, confirm_password } = this.state;
        let { fetching , error } = this.props;
        return (
            <div className="m-login__signin">
                <div className="m-login__head">
                    <h3 className="m-login__title">Reset Password</h3>
                    <div className="m-login__desc">Enter your New &amp; Password.</div>
                </div>
                <form className="m-login__form m-form" onSubmit={this.handleSubmit}>
                    <div className="form-group m-form__group">
                        <input className="form-control m-input m-login__form-input--last" type="password" value={password} placeholder="New Password" name="password" onChange={this.handleChange} />
                        {password_err && <p className="errtxt">Password must atleast 6 characters</p>}
                    </div>
                    <div className="form-group m-form__group">
                        <input className="form-control m-input m-login__form-input--last" type="password" placeholder="Confirm Password" name="confirm_password" onChange={this.handleChange} value={confirm_password} />
                        {confirm_password_err && <p className="errtxt">{confirm_password_err}</p>}
                    </div>
                    {error && <p className="errtxt">{ajaxerrmsg(error)}</p>}
                    <div className="m-login__form-action">
                        <button type="submit" className="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air whitecolor" onSubmit={this.handleSubmit}  disabled={ confirm_password && password && !fetching ? false : true }>Reset Password  {fetching && <div className="sp-circle"></div>}</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default connect((state) => state.user)(ResetForm)