import React, { Component } from 'react'
import { Link } from "react-router-dom"
import { urls, ajaxerrmsg } from '../../actions/common';
import axios from "axios";


export default class ForgotForm extends Component {

    constructor(props){
        super(props);
        this.state = {
            email: '',
            email_err: '',
            fetching: false,
            success: false,
            error: false,
        };
        this.handleChange = this.handleChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this); 
        this.forgot = this.forgot.bind(this); 
    }

    handleChange(e){
        let {name, value} = e.target;
        this.setState({[name]: value, [name + "_err"]: false});
    }

    handleSubmit(e){
        e.preventDefault();
        let {email} = this.state;
        this.forgot({username: email});
    }

    async forgot(data){
        this.setState({fetching: true});
        try {
            let res = await axios({
                url: urls.forgotpwd,
                method: "post",
                headers: urls.authheader,
                data: JSON.stringify(data)
            });
            if(res){
                console.log(res);
                this.setState({fetching: false});            
                this.setState({success: res.data.message});            
            }
        } catch(error){
            this.setState({fetching: false});            
            this.setState({error: ajaxerrmsg(error.response.data)});            
        }
    }

    render() {
        let { email, email_err, fetching, success, error} = this.state;
        return (
            <div className="m-login__forget-password">
                <div className="m-login__head">
                    <h3 className="m-login__title">Forgotten Password ?</h3>
                    <div className="m-login__desc">Enter your username to reset your password:</div>
                </div>
                <form className="m-login__form m-form" onSubmit={this.handleSubmit}>
                    <div className="form-group m-form__group">
                        <input className="form-control m-input" type="text" placeholder="Username" name="email" id="m_email" autoComplete="off" onChange={this.handleChange} />
                        {email_err && <div className="errtxt">Username is invalid</div>}
                    </div>
                    {success && <p className="theme-color-text">{success}</p>}
                    {error && <p className="errtxt">{error}</p>}
                    <div className="m-login__form-action">
                        <button onSubmit={this.handleSubmit} className="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air mr-1" type="submit" disabled={ email && !fetching ? false : true}>Request {fetching && <div className="sp-circle"></div>}</button>
                        <Link to="/login" className="btn btn-outline-focus m-btn m-btn--pill m-btn--custom theme-color-text">Cancel</Link>
                    </div>
                </form>
            </div>
        )
    }
}
