import React from 'react'
import bg from "../../assets/app/media/img/bg/bg-4.jpg"
import ForgotForm from './forgotform';

const ForgotPage = props => (
    <div className={`m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--forget-password`} id="m_login">
        <div className="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content m-grid-item--center hidden-xs" style={{ backgroundImage: `url(${bg})`, height: `100vh` }}>
            <div className="m-grid__item">
                <h3 className="m-login__welcome">Join Our Community</h3>
                <p className="m-login__msg">
                    Refer many members into the system and start earning. 
                </p>
            </div>
        </div>
        <div className="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
            <div className="m-stack m-stack--hor m-stack--desktop">
                <div className="m-stack__item m-stack__item--fluid">
                    <div className="m-login__wrapper mobilep15">
                        <div className="m-login__logo mb10">
                            <h2 align="center" className="mlmlogotext">KPS TOURS</h2>
                        </div>
                        <ForgotForm {...props}/>
                    </div>
                </div>
            </div>
        </div>
    </div>
)

export default ForgotPage
