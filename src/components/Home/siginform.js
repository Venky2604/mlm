import React, { Component } from 'react'
import { Link } from "react-router-dom"
import { pwdvalid, login, ajaxerrmsg, saveLocalState } from '../../actions/common';
import { connect } from "react-redux";

class SiginForm extends Component {

    constructor(props){
        super(props);
        this.state = {
            username: '',
            username_err: false,
            password: '',
            password_err: false
        };
        this.handleChange = this.handleChange.bind(this); 
        this.handleSubmit = this.handleSubmit.bind(this); 
    }

    handleChange(e){
        let {name, value} = e.target;
        this.setState({[name]: value, [name + "_err"]: false});
    }

    handleSubmit(e){
        e.preventDefault();
        let {username, password} = this.state;
        if(!pwdvalid(password)){
            this.setState({password_err: true});
            return;
        }
        this.props.dispatch(login({
            username:username, password
        }))
    }

    componentWillReceiveProps(nextProps){
        let { data, error, history } = nextProps;
        if(!error && data.bearer_token){
            saveLocalState(data);
            history.replace("/");
        }
    }

    render() {
        let { username, username_err, password, password_err } = this.state;
        let { fetching , error } = this.props;
        return (
            <div className="m-login__signin">
                <div className="m-login__head">
                    <div className="m-login__desc">Enter your Username &amp; Password to login.</div>
                </div>
                <form className="m-login__form m-form" onSubmit={this.handleSubmit}>
                    <div className="form-group m-form__group">
                        <input className="form-control m-input" type="text" placeholder="Username" name="username" autoComplete="off" onChange={this.handleChange} value={username} />
                        {username_err && <p className="errtxt">Username is required</p>}
                    </div>
                    <div className="form-group m-form__group">
                        <input className="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password"  value={password} onChange={this.handleChange}/>
                        {password_err && <p className="errtxt">Password must be atleast 6 characters</p>}
                    </div>
                    {error && <p className="errtxt">{ajaxerrmsg(error)}</p>}
                    <div className="row m-login__form-sub">
                        <div className="col m--align-left">
                            
                        </div>
                        <div className="col m--align-right">
                            <Link to="/forgot-password" id="m_login_forget_password" className="m-link">Forgot Password ?</Link>
                        </div>
                    </div>
                    <div className="m-login__form-action">
                        <button className="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air whitecolor" onSubmit={this.handleSubmit} type="submit" disabled={ username && password && !fetching ? false : true }>Sign In {fetching && <div className="sp-circle"></div>}</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default connect((state)=> state.user)(SiginForm)