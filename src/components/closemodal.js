import React, { Component } from 'react'
import { closeModal } from '../actions/common'
import {connect} from "react-redux"

class ModalClose extends Component {
    render() {
        return (
            <button type="button" className={this.props.className} onClick={() => this.props.dispatch(closeModal())}>
                {this.props.children}
            </button>
        )
    }
}

export default connect(null)(ModalClose)
