import React, { Component } from 'react'
import Modal from './modal';
import {connect} from "react-redux"
import AssignPinModal from './epins/assignpinmodal';
import GeneratePinModal from './epins/generatepinmodal';
import AddStaff from './users/addStaff';
import AddUserModal from './users/addusermodal';
import ResetUserPwd from './users/resetuserpwd';
import UserDetailsModal from './users/userdetailsmodal';
import UpdateUserModal from './users/updateusermodal';
import PayModal from './reports/paymodal';
import UserSuccModal from './Tree/usersuccmodal';
import ChangePwd from './Mainlayout/changepwd';

class CenterModal extends Component {

    chooseModal(name){
        switch(name){
            case 'ASSIGN_PIN': 
                return <AssignPinModal />
            case 'GENERATE_PIN':
                return <GeneratePinModal />
            case 'ADD_ADMIN_MODAL':
                return <AddStaff {...this.props} />
            case 'ADD_USER_MODAL':
                return <AddUserModal {...this.props} />
            case "RESET_MODAL":
                return <ResetUserPwd  {...this.props} />
            case "USER_DETAILS_MODAL":
                return <UserDetailsModal {...this.props} />
            case "UPDATE_USER_MODAL":
                return <UpdateUserModal {...this.props} />
            case "PAY_MODAL":
                return <PayModal {...this.props} />
            case "USER_SUCC":
                return <UserSuccModal {...this.props} />
            case "CHANGE_PWD":
                return <ChangePwd {...this.props} />
            default:
                return null
        }
    }

    render() {
        return (
        <Modal>
            {this.props.center ? 
                <div className="modal fade show" id="epinmodal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style={{paddingRight: `15px`, display: `block`}}>
                    <div className="modal-dialog modal-dialog-centered" role="document">
                        {this.chooseModal(this.props.modalName)}
                    </div>
                </div> : 
                <div className="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light m-quick-sidebar--on">
                    {this.chooseModal(this.props.modalName)}
                </div>
            }
        </Modal>
        )
    }
}

export default connect((state) => state.modalReducer)(CenterModal)