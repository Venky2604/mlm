import React, { Component } from 'react'
import {connect} from "react-redux"
import axios from "axios"
import { urls, dateTimeFormat } from '../../actions/common';

class DashboardPage extends Component {

    constructor(props){
        super(props);
        this.state = {
            fetching: false,
            data: {}
        };
        this.getData = this.getData.bind(this);
    }

    componentDidMount(){
        this.getData();
    }

    async getData(){
        this.setState({fetching: true});
        let user = this.props.user.user;
        try{
            let res = await axios.get(
                (user.role.id === 5 ? `${urls.listUsers}/${user.id}` : urls.dashboard), 
            {
                headers: {
                    "Accept": "application/json",
                    "Authorization": `Bearer ${this.props.user.bearer_token}`
                }
            })
            this.setState({fetching: false});       
            this.setState({data: res.data});
        } catch(error) {
            this.setState({fetching: false});       
            console.log(error.response);
        }
    }

    render() {
        if(this.state.fetching) return (
            <div className="flex-center" style={{height: `100vh`, width: `100%`}}><div className="listingloader"></div></div>
        );
        if(this.props.user.user.role.id === 5){
            let {downline_count, auto_filling_count, is_salary_eligible, is_tour_eligible, created_at, level_achieved } = this.state.data;
            return (
                <div className="m-grid__item m-grid__item--fluid m-wrapper p20">
                <h4 className="mb-3">{new Date().toLocaleDateString("en-IN", { year: 'numeric', month: 'long'})}</h4>
                <div className="m-portlet">
                    <div className="m-portlet__body  m-portlet__body--no-padding">
                        <div className="row m-row--no-padding m-row--col-separator-xl">
                            <Widget title="Direct Line" count={downline_count}/>
                            <Widget title="Level" count={level_achieved} />
                            <Widget title="Tours" count={is_tour_eligible ? "Yes" : "NO"} colorClassName={is_tour_eligible ? "m--font-success" : "m--font-danger"} />
                            <Widget title="Salary" count={is_salary_eligible ? "Yes" : "NO"}
                            colorClassName={is_salary_eligible ? "m--font-success" : "m--font-danger"} />
                        </div>
                    </div>
                </div>
                <h4 className="mb-3">Auto Filling</h4>
                <div className="m-portlet">
                    <div className="m-portlet__body  m-portlet__body--no-padding">
                        <div className="row m-row--no-padding m-row--col-separator-xl">
                            <Widget title="Auto Filling" count={auto_filling_count} colorClassName="m--font-accent"/>
                            <Widget title="Joined On" count={dateTimeFormat(created_at)} large={true}/>
                        </div>
                    </div>
                </div>
            </div>
            )
        }
        let { auto_filling_achievers, auto_filling_payout, monthly_entries, monthly_pay_in, monthly_salary_eligible, monthly_tour_eligible, total_entries, total_pay_in } = this.state.data;
        return (
            <div className="m-grid__item m-grid__item--fluid m-wrapper p20">
                <h4 className="mb-3">{new Date().toLocaleDateString("en-IN", { year: 'numeric', month: 'long'})}</h4>
                <div className="m-portlet">
                    <div className="m-portlet__body  m-portlet__body--no-padding">
                        <div className="row m-row--no-padding m-row--col-separator-xl">
                            <Widget title="No of Entries" count={monthly_entries}/>
                            <Widget title="Pay In" count={monthly_pay_in} currency={true}/>
                            <Widget title="Tours" count={monthly_tour_eligible}/>
                            <Widget title="Salary" count={monthly_salary_eligible}/>
                        </div>
                    </div>
                </div>
                <h4 className="mb-3">Auto Filling</h4>
                <div className="m-portlet">
                    <div className="m-portlet__body  m-portlet__body--no-padding">
                        <div className="row m-row--no-padding m-row--col-separator-xl">
                            <Widget title="No of Entries" count={total_entries}/>
                            <Widget title="Achievers" count={auto_filling_achievers}/>
                            <Widget title="Pay Out" count={auto_filling_payout} currency={true}/>
                        </div>
                    </div>
                </div>
                <h4 className="mb-3">Overall</h4>
                <div className="m-portlet">
                    <div className="m-portlet__body  m-portlet__body--no-padding">
                        <div className="row m-row--no-padding m-row--col-separator-xl">
                            <Widget title="Total Entries" count={total_entries} />
                            <Widget title="Total Pay In" count={total_pay_in} currency={true} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const Widget = ({title, count, currency, colorClassName="m--font-brand", large = false}) => (
    <div className={`col-md-12 col-lg-6 ${large ? "col-xl-6" : "col-xl-3"}`}>
        <div className="m-widget24">
            <div className="m-widget24__item">
                <h4 className="m-widget24__title mt-3">
                    {title}
                </h4>
                <br />
                <h3 className={`m-widget24__title ${colorClassName} mt-3 f30`}>
                    {count ? currency ? count.toLocaleString('en-IN', { style: 'currency', currency: 'INR', minimumFractionDigits: 0 }) : count : 0}
                </h3>
            </div>
        </div>
    </div>
)

const mapStateToProps = (state) => {
    return { 
        user: state.user.data
    }
}

export default connect(mapStateToProps)(DashboardPage)





