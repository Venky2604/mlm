import {
    combineReducers
} from 'redux';
import {
    modalReducer,
    user,
    epins,
    listUsers,
    FormOptions,
    userProfile,
    listLevels,
    listLevelDetail,
    autoFill,
    listAuto,
    listAutoDetail,
    listPayoutDetail,
    listTourDetail,
    listSalaryDetail
} from "./common";

export default combineReducers({
    modalReducer,
    user,
    epins,
    listUsers,
    FormOptions,
    userProfile,
    listLevels,
    listLevelDetail,
    autoFill,
    listAuto,
    listAutoDetail,
    listPayoutDetail,
    listTourDetail,
    listSalaryDetail
})