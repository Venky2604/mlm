import { loadLocalState } from "../actions/common"

export const modalReducer = (state = { open: false }, action) => {
    let { type, payload } = action;
    switch(type) {
        case 'OPEN_MODAL': 
            return {...state,open: true, ...payload}
        case 'CLOSE_MODAL': 
            return {...state,open: false}
        default:
            return state
    }
}


export const toastReducer = (state = { show: false }, action) => {
    switch(action.type) {
        case 'SHOW_TOAST':
            return {...state, show: true, msg: action.msg, error: action.error}
        case 'HIDE_TOAST':
            return {...state, show: false}
        default:
            return state;
    }
}

let userInitialState = {
    fetching: false,
    error: false,
    data: {}
};

const userdata = loadLocalState();

if(userdata){
    userInitialState = {...userInitialState, data: userdata}    
}

export const user = (state = userInitialState, action) => {
    switch(action.type){
        case "LOGIN_PENDING":
            return {...state, fetching: true, data: {}, error: false}
        case "LOGIN_FULFILLED": 
            return {...state, fetching: false, data: action.payload.data , error: false}
        case "LOGIN_REJECTED":
            return {...state, fetching: false, data: {}, error : action.payload.response ? action.payload.response.data : '' }
        case "RESET_PWD_PENDING":
            return {...state, fetching: true, data: {}, error: false}
        case "RESET_PWD_FULFILLED": 
            return {...state, fetching: false, data: action.payload.data , error: false}
        case "RESET_PWD_REJECTED":
            return {...state, fetching: false, data: {}, error : action.payload.response ? action.payload.response.data : '' }
        case "CLEAR_USER":
            return {...state, fetching: false, data: {}, error: false}
        default:
            return {...state}
    }
}

export const epins = (state = {data: [], fetching: false , error: false} , action) => {
    switch(action.type){
        case "LIST_EPINS_PENDING":
            return {...state, fetching: true, data: [], error: false}
        case "LIST_EPINS_FULFILLED": 
            let res = action.payload.data;
            return {...state, fetching: false, data: res.data , error: false, current_page: res.current_page, last_page: res.last_page}
        case "LIST_EPINS_REJECTED":
            return {...state, fetching: false, data: [], error : true }
        case "SELECT_EPIN":
         return {...state, selected: state.data.filter(pin => pin.id === action.id) }
        case "CLEAR_EPINS":
            return {...state, data: [], fetching: false , error: false}
        default:
            return {...state}
    }
}

export const listUsers = (state = {data: [], fetching: false , error: false} , action) => {
    switch(action.type){
        case "LIST_USERS_PENDING":
            return {...state, fetching: true, data: [], error: false, userselected: null }
        case "LIST_USERS_FULFILLED": 
            let res = action.payload.data;
            return {...state, fetching: false, data: res.data , error: false, current_page: res.current_page, last_page: res.last_page}
        case "LIST_USERS_REJECTED":
            return {...state, fetching: false, data: [], error : true}
        case "SELECT_LIST_USER": 
            return {...state, userselected: state.data.filter(x => x.id === action.id)[0]}
        case "CLEAR_LIST_USER":
            return {...state, fetching: false, data: [], error: false, userselected: null}
        case "UPDATE_USER":
            return {
                ...state,
                data: state.data.map(x => 
                    (x.id === action.payload.id) ?
                    {...x, ...action.payload} :
                    x
                ),
                userselected: null
            }
        default:
            return {...state}
    }
}

// export const listStaffs = (state = {data: [], fetching: false , error: false} , action) => {
//     switch(action.type){
//         case "LIST_STAFF_PENDING":
//             return {...state, fetching: true, data: [], error: false, userselected: {}}
//         case "LIST_STAFF_FULFILLED": 
//             let res = action.payload.data;
//             return {...state, fetching: false, data: res.data , error: false, current_page: res.current_page, last_page: res.last_page}
//         case "LIST_STAFF_REJECTED":
//             return {...state, fetching: false, data: [], error : true}
//         case "SELECT_LIST_STAFF": 
//             return {...state, userselected: state.data.filter(x => x.id === action.id)[0]}
//         case "CLEAR_LIST_STAFF":
//             return {...state, fetching: false, data: [], error: false, userselected: {}}
//         default:
//             return {...state}
//     }
// }

// export const listAdmin = (state = {data: [], fetching: false , error: false} , action) => {
//     switch(action.type){
//         case "LIST_ADMIN_PENDING":
//             return {...state, fetching: true, data: [], error: false}
//         case "LIST_ADMIN_FULFILLED": 
//             let res = action.payload.data;
//             return {...state, fetching: false, data: res.data , error: false, current_page: res.current_page, last_page: res.last_page}
//         case "LIST_ADMIN_REJECTED":
//             return {...state, fetching: false, data: [], error : true}
//         case "CLEAR_LIST_ADMIN":
//             return {...state, fetching: false, data: [], error: false}
//         default:
//             return {...state}
//     }
// }


export const listLevels = (state = {data: [], fetching: false , error: false} , action) => {
    switch(action.type){
        case "LIST_LEVELS_PENDING":
            return {...state, fetching: true, data: [], error: false}
        case "LIST_LEVELS_FULFILLED": 
            let res = action.payload.data;
            return {...state, fetching: false, data: res, error: false}
        case "LIST_LEVELS_REJECTED":
            return {...state, fetching: false, data: [], error : true}
        case "LIST_LEVELS_CLEAR":
            return {...state, fetching: false, data: [], error : false}
        default:
            return {...state}
    }
}

export const listAuto = (state = {data: [], fetching: false , error: false} , action) => {
    switch(action.type){
        case "LIST_AUTO_PENDING":
            return {...state, fetching: true, data: [], error: false}
        case "LIST_AUTO_FULFILLED": 
            let res = action.payload.data;
            return {...state, fetching: false, data: res, error: false}
        case "LIST_AUTO_REJECTED":
            return {...state, fetching: false, data: [], error : true}
        case "LIST_AUTO_CLEAR":
            return {...state, fetching: false, data: [], error : false}
        default:
            return {...state}
    }
}

export const listLevelDetail = (state = {data: [], fetching: false , error: false} , action) => {
    switch(action.type){
        case "LIST_LEVELS_DETAIL_PENDING":
            return {...state, fetching: true, data: [], error: false}
        case "LIST_LEVELS_DETAIL_FULFILLED": 
            let res = action.payload.data;
            return {...state, fetching: false, data: res, error: false}
        case "LIST_LEVELS_DETAIL_REJECTED":
            return {...state, fetching: false, data: [], error : true}
        case "LIST_LEVELS_DETAIL_CLEAR":
            return {...state, fetching: false, data: [], error : false}
        default:
            return {...state}
    }
}

export const listTourDetail = (state = {data: [], fetching: false , error: false} , action) => {
    switch(action.type){
        case "LIST_TOURS_PENDING":
            return {...state, fetching: true, data: [], error: false}
        case "LIST_TOURS_FULFILLED": 
            let res = action.payload.data;
            return {...state, fetching: false, data: res, error: false}
        case "LIST_TOURS_REJECTED":
            return {...state, fetching: false, data: [], error : true}
        case "LIST_TOURS_CLEAR":
            return {...state, fetching: false, data: [], error : false}
        default:
            return {...state}
    }
}


export const listSalaryDetail = (state = {data: [], fetching: false , error: false} , action) => {
    switch(action.type){
        case "LIST_SALARY_PENDING":
            return {...state, fetching: true, data: [], error: false}
        case "LIST_SALARY_FULFILLED": 
            let res = action.payload.data;
            return {...state, fetching: false, data: res, error: false}
        case "LIST_SALARY_REJECTED":
            return {...state, fetching: false, data: [], error : true}
        case "LIST_SALARY_CLEAR":
            return {...state, fetching: false, data: [], error : false}
        default:
            return {...state}
    }
}

// export const listTourMonthDetail = (state = {data: [], fetching: false , error: false} , action) => {
//     switch(action.type){
//         case "LIST_TOURS_MONTH_PENDING":
//             return {...state, fetching: true, data: [], error: false}
//         case "LIST_TOURS_MONTH_FULFILLED": 
//             let res = action.payload.data;
//             return {...state, fetching: false, data: res, error: false}
//         case "LIST_TOURS_MONTH_REJECTED":
//             return {...state, fetching: false, data: [], error : true}
//         case "LIST_TOURS_MONTH_CLEAR":
//             return {...state, fetching: false, data: [], error : false}
//         default:
//             return {...state}
//     }
// }


export const listAutoDetail = (state = {data: [], fetching: false , error: false} , action) => {
    switch(action.type){
        case "LIST_AUTO_DETAIL_PENDING":
            return {...state, fetching: true, data: [], error: false}
        case "LIST_AUTO_DETAIL_FULFILLED": 
            let res = action.payload.data;
            return {...state, fetching: false, data: res, error: false}
        case "LIST_AUTO_DETAIL_REJECTED":
            return {...state, fetching: false, data: [], error : true}
        case "LIST_AUTO_DETAIL_CLEAR":
            return {...state, fetching: false, data: [], error : false}
        default:
            return {...state}
    }
}

export const listPayoutDetail = (state = {data: [], fetching: false , error: false} , action) => {
    switch(action.type){
        case "PAYOUT_LIST_PENDING":
            return {...state, fetching: true, data: [], error: false}
        case "PAYOUT_LIST_FULFILLED": 
            let res = action.payload.data;
            return {...state, fetching: false, data: res, error: false}
        case "PAYOUT_LIST_REJECTED":
            return {...state, fetching: false, data: [], error : true}
        case "PAYOUT_LIST_CLEAR":
            return {...state, fetching: false, data: [], error : false}
        case "PAY_SELECT":
            return {
                ...state,
                selected: state.data.filter(x => x.id === action.id)[0]
            }
        default:
            return {...state}
    }
}


export const FormOptions = (state = {
    fetching: false , error: false, epins: [],  cities: []
}, action) => {
    switch(action.type){
        case "FORM_OPTIONS_PENDING":
            return {...state, fetching: true, epins: [], users: [] , error: false}
        case "FORM_OPTIONS_FULFILLED":  
            let res = action.payload;
            return {...state, fetching: false, users: res[0].data.map(x => ({id: x.id, name: x.user_profile.name})), cities: res[1].data.cities, error: false}
        case "FORM_OPTIONS_REJECTED":
            return {...state, fetching: false, epins: [], users: [] , error : true}
        default:
            return {...state}
    }
}

export const userProfile = ( state = {
    fetching: false, error: false, data: {}
}, action ) => {
    switch(action.type){
        case "USER_PROFILE_PENDING":
            return {...state, fetching: true, data: {}, error: false}
        case "USER_PROFILE_FULFILLED": 
            return {...state, fetching: false, data: action.payload.data, error: false }
        case "USER_PROFILE_REJECTED":
            return {...state, fetching: false, data: {}, error: true }
        default:
            return {...state}
    }
}

export const autoFill = ( state = {
    fetching: false, error: false, data: []
}, action ) => {
    switch(action.type){
        case "AUTO_FILL_LIST_PENDING":
            return {...state, fetching: true, data: [], error: false}
        case "AUTO_FILL_LIST_FULFILLED": 
            return {...state, fetching: false, data: action.payload.data, error: false }
        case "AUTO_FILL_LIST_REJECTED":
            return {...state, fetching: false, data: [], error: true }
        default:
            return {...state}
    }
}